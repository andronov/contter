GRAPHQL CHAT API
================

#### Получение списка чатов
Запрос:
```
query allThread($offset: Int, $limit: Int) {
      threads(offset: $offset, limit: $limit)  {
        id,
        name,
        created
        members {
            id,
            phone
        }
      }
}

$offset - с какого индекса выводить сообщения. По умолчанию 0
$limit - количество сообщений. По умолчанию 15
```


#### Создание чата
Запрос:
```
mutation CreateThread($type: Int, $members: [Int], $message: Int, $name: String) {
        createThread(type: $type, members: $members, message: $message, name: $name) {
          thread {
              id,
                name,
                created,
                members {
                    id,
                    phone
                }
          }
        }
        }

$type - тип сообщение.
$members - список пользователей, массив из ид.
$message - связь с сообщением.
$name - название чата. В случае если из сообщения чат, то название от текста сообщения.
```

#### Получение сообщений
Запрос:
```
query getMessage($offset: Int, $limit: Int, $thread: Int!) {
      messages(offset: $offset, limit: $limit, thread: $thread)  {
        id,
        created,
        text,
        recipient,
        file,
        location,
        parent {
            id,
            created
        },
        status,
        sender {
            id,
            phone,
            firstName,
            lastName,
            email,
            avatar
        },
        chips {
          id,
          created,
          value {
            id,
            value
          }
          chip {
            id,
            name
          }
        }
      }
}
$offset - с какого индекса выводить сообщения. По умолчанию 0.
$limit - количество сообщений. По умолчанию 15.
*$thread - ид чата.
```

#### Проверка username на уникальность
Запрос:
```
mutation CheckUsername($username: String!) {
    checkUsername(username: $username)  {
        ok
    }
}
$username - username.
```


#### Создание сообщения
Запрос:
```
mutation CreateMessage($recipient: Int!, $parent: Int, $text: String, $location: String, $file: String) {
        createMessage(recipient: $recipient, parent: $parent, text: $text, location: $location, file: $file) {
          message {
              id,
              created,
              text,
              recipient,
              file,
              location,
              parent,
              sender
          }
        }
}

*$recipient - получатель. ид thread.
$parent - родитель сообщения.
$text - текст сообщения.
$location - геолокация координаты.
$file - путь до файла. Файл передавать другим запросом.
```

#### Редактирование сообщений
Запрос:
```
mutation EditMessage($id: Int!, $parent: Int, $text: String, $location: String, $file: String) {
    editMessage(id: $id, parent: $parent, text: $text, location: $location, file: $file) {
        message {
            id,
                created,
                text,
                recipient,
                file,
                location,
                parent {
                id
            }
            sender {
                id
            }
        }
    }
}
*$id - ИД сообщение.
$parent - родитель сообщения.
$text - текст сообщения.
$location - геолокация координаты.
$file - путь до файла. Файл передавать другим запросом.
```

#### Получение списка пользователей (пока всех)
Запрос:
```
allUsers {
users {
        id,
        phone,
        firstName,
        lastName,
        email,
        avatar
      }
}
```

#### Создание пользователя
Запрос:
```
mutation CreateUser {
          createUser(
            phone: "78912924485"
          ) {
            user {
            id,
            phone
            }
          }
}
```

#### Получение чипсов
Запрос:
```
query getChips($offset: Int, $limit: Int, $type: Int, $query: String) {
      chips(offset: $offset, limit: $limit, type: $type, query: $query)  {
        id,
        name,
        type,
        double,
        values {
            id,
            value,
            creator
        }
      }
}
$type - тип чипса.
$query - запрос, например: "Ве" - вернет все чипсы начинающиеся с этого.
$offset - с какого индекса выводить сообщения. По умолчанию 0.
$limit - количество сообщений. По умолчанию 15.
```

#### Создание значения чипса
Запрос:
```
mutation CreateChipValue($chip: Int!, $value: String!) {
    createChipValue(chip: $chip, value: $value) {
      chipValue {
          id,
          chip,
          value,
          creator
      }
    }
}
*$chip - ид чипса.
*$value - название значения чипса.
```

#### Редактирование чипса значение
Запрос:
```
mutation EditChipValue($id: Int!, $value: String!) {
        editChipValue(id: $id, value: $value) {
          chipValue {
              id,
              chip,
              value,
              creator
          }
        }
}
*$id - ид значение чипса.
*$value - название значения чипса.
```

#### Добавление чипса к сообщению
Запрос:
```
mutation CreateChipMessage($chip: Int!, $value: Int, $message: Int!) {
        createChipMessage(chip: $chip, value: $value, message: $message) {
            chipMessage {
                id,
                chip,
                value,
                message,
                creator
                __typename
            }
        }
}
*$chip - ид чипса.
*$value - ид значение чипса.
*$message - ид сообщения.
```

#### Удаление чипса из сообщения
Запрос:
```
mutation DeleteChipMessage($chip: Int!, $message: Int!) {
        deleteChipMessage(chip: $chip, message: $message) {
            ok
        }
}
*$chip - ид чипса.
*$message - ид сообщения.
```

#### Получение товаров для ИМ
Запрос:
```
query getProducts($offset: Int, $limit: Int, $seller: Int!) {
    products(offset: $offset, limit: $limit, seller: $seller)  {
        id,
            created,
            name,
            weight,
            price,
            img,
            seller
    }
}
$seller - ИД продавца.
$offset - с какого индекса выводить сообщения. По умолчанию 0.
$limit - количество сообщений. По умолчанию 15.
```

#### Создание корзины ИМ
Запрос:
```
mutation createCart($seller: Int!) {
    createCart(seller: $seller) {
        cart {
            id
        }
    }
}
$seller - ИД продавца.
```

#### Получение корзин/корзины текущего пользователя
Запрос:
```
query getCarts($id: Int) {
    carts(id: $id)  {
        id,
            created,
            buyer,
            seller,
            total,
            products {
            id,
                product {
                name,
                    id
            },
            quantity
        },
        message,
            status
    }
}
$id - ИД корзины.
```

#### Добавление товара в корзину
Запрос:
```
mutation addProductToCart($cart: Int!, $product: Int!, $quantity: Int, $id: Int) {
        addProductToCart(cart: $cart, product: $product, quantity: $quantity, id: $id) {
            cart {
               id,
               created,
               buyer,
               seller,
               total,
               products {
                 id,
                 product {
                   name,
                   id
                 },
                 quantity
               },
               message,
               status
             }
        }
    }
$cart - ИД корзины.
$product - ИД продукта.
$quantity - Количество.
$id - ИД cart_product.
Передавать ид если отредактировать нужно количество
```

#### Удаление товара из корзины
Запрос:
```
mutation deleteProductFromCart($cart: Int!, $id: Int!) {
    deleteProductFromCart(cart: $cart, id: $id) {
        cart {
            id,
                created,
                buyer,
                seller,
                total,
                products {
                id,
                    product {
                    name,
                        id
                },
                quantity
            },
            message,
                status
        }
    }
}
$cart - ИД корзины.
$id - ИД cart_product.
```

#### Редактирование магазина
Запрос:
```
mutation EditShop($id: Int!, $name: String, $username: String, $desc: String, $avatar: String) {
    editShop(id: $id, name: $name, username: $username, desc: $desc, avatar: $avatar) {
        user {
            id,
            phone,
            type,
            firstName,
            lastName,
            username,
            name,
            desc,
            email,
            avatar
        },        
        creator {
            id,
            phone,
            firstName,
            lastName,
            username,
            name,
            desc,
            email,
            avatar,
            shops {
                id,
                username,
                name,
                desc
            }
        }
    }
}
$id - ИД  магазина.
$name - Название магазина.
$username - Логин магазина.
$desc - Описание магазина.
$avatar - Аватар магазина.
```

#### Создание магазина
Запрос:
```
mutation CreateShop($name: String!, $username: String!, $desc: String) {
    createShop(name: $name, username: $username, desc: $desc) {
        user {
            id,
            phone,
            type,
            firstName,
            lastName,
            username,
            name,
            desc,
            email,
            avatar
        },
        thread {
            id,
            name,
            created,
            members {
                id,
                phone,
                name
            }
        },        
        creator {
            id,
            phone,
            firstName,
            lastName,
            username,
            name,
            desc,
            email,
            avatar,
            shops {
                id,
                username,
                name,
                desc
            }
        }
    }
}
$name - Название магазина.
$username - Логин магазина.
$desc - Описание магазина.
```


#### Редактирование пользователя
Запрос:
```
mutation EditUser($name: String, $username: String, $desc: String, $avatar: String, $type: Int) {
        editUser(name: $name, username: $username, desc: $desc, avatar: $avatar, type: $type) {
            user {
                id,
                phone,
                type,
                firstName,
                lastName,
                username,
                name,
                desc,
                email,
                avatar
            }
        }
}
$name - Имя юзера.
$username - Логин юзера.
$desc - Описание юзера.
$avatar - Аватар юзера.
```

#### Получение данных о юзере
Запрос:
```
query getUser($id: Int!){
    user(id: $id) {
        id,
            phone,
            firstName,
            lastName,
            name,
            desc,
            email,
            avatar
    }
}
$id = ИД пользователя
```

#### Поиск по контактам
Запрос:
```
query Search($query: String!){
    search(query: $query) {
        id,
            phone,
            firstName,
            lastName,
            username,
            email,
            avatar
    }
$query - Поисковой запрос
```

#### Поиск по сообщениям
Запрос:
```
query SearchMessage($query: String!, $offset: Int, $limit: Int){
      searchMessage(query: $query, offset: $offset, limit: $limit) {
        id,
        created,
        text,
        recipient,
        file,
        location,
        parent {
            id,
            created
        },
        status,
        sender {
            id,
            phone,
            firstName,
            lastName,
            email,
            avatar
        },
        chips {
          id,
          created,
          value {
            id,
            value
          }
          chip {
            id,
            name
          }
        }
      }
    }
$query - Поисковой запрос
$offset - с какого индекса выводить сообщения. По умолчанию 0
$limit - количество сообщений. По умолчанию 30
```

```
* - обязательное поле
```

## Структура graphql
```
// Пользователь
type User {
  updated: String
  created: String
  id: ID!
  phone: String
  is_active: Boolean
}

type Chip {
  updated: String
  created: String
  id: ID!
  name: String
  type: Int // 1 - default, 2 - task
}
type ChipValue {
  updated: String
  created: String
  id: ID!
  creator: User
  chip: Chip!
  value: String
}
// Чипсы в сообщении
type ChipMessage {
  updated: String
  created: String
  id: ID!
  message: Message
  chip: Chip
  value: ChipValue
}

// Чаты
type Thread {
  updated: String
  created: String
  id: ID!
  name: string
  type: Int // 1-one-to-one чат, 2-групповой
  creator: User // Создать чата
  message: Message
  members: [User] // Участники
}

// Сообщение
type Message {
  updated: String
  created: String
  id: ID!
  status: Int // default - 1
  text: string
  file: string
  location: string
  sender: User! 
  recipient: Thread
  parent: Message
}

// Товар
type Product {
  updated: Date
  created: Date
  id: ID!
  name: String   // Название
  weight: Float  // Масса
  price: Float   // Цена
  img: String    // Иконка
  seller: User!  // Продавец
}

// Товары в корзине
type ProductCart {
  updated: Date
  created: Date
  id: ID!
  product: Product! // Продукт
  quantity: Number  // Количество
}

// Корзина
type Cart {
  updated: Date
  created: Date
  id: ID!
  products [ProductCart] // Позиции в корзине
  total: Float     // Общая сумма
  buyer: User!     // Покупатель
  seller: User!    // Продавец
  status: Number!  // Статус
  message: Message // Сообщение
}
```
