const NODE_ENV = process.env.NODE_ENV;
const DEBUG = true;process.env.DEBUG;
const isCordova = process.env.TARGET.indexOf('cordova') >= 0;
const { assets } = serviceWorkerOption;

//noinspection JSAnnotator
self.document = {};
self.ids = null;
let mqttClient = null;
let currentUser = null;
const ntf = null;
let attemps = 0;

const CACHE_NAME = (new Date()).toISOString();
const assetsToCache = assets
  .concat(['./'])
  .map((path) => { return new URL(path, location).toString(); });
if (NODE_ENV === 'production' && TARGET !== 'cordova') {
  self.addEventListener('install', (event) => {
    event.waitUntil(
      global.caches
        .open(CACHE_NAME)
        .then((cache) => { return cache.addAll(assetsToCache); })
        .then(() => {
          if (DEBUG) {
            console.log('Cached assets: main', assetsToCache);
          }
        })
        .catch((err) => {
          if (DEBUG) {
            console.error(err);
          }
          throw err;
        }),
    );
  });
}

self.addEventListener('activate', (event) => {
  if (DEBUG) {
    console.log('[SW] Activate event');
  }
  event
    .waitUntil(self.clients.claim());

  event
    .waitUntil(
      global.caches
        .keys()
        .then((cacheNames) => {
          return Promise.all(
          cacheNames.map((cacheName) => { return cacheName.indexOf(CACHE_NAME) === 0; })
            ? null
            : global.caches.delete(cacheName),
        );
        }),
    );
});

self.addEventListener('message', (event) => {
  console.log('event', event);
  switch (event.data.action) {
    case 'sendDataServiceWorker':
      currentUser = event.data.currentUser;
      handlerDataServiceWorker(event);
      break;
    case 'skipWaiting':
      if (self.skipWaiting) {
        self.skipWaiting();
        self.clients.claim();
      }
      break;
    default: break;
  }
});

self.addEventListener('fetch', (event) => {
  const request = event.request;

  // Ignore not GET request.
  if (request.method !== 'GET') {
    if (DEBUG) {
      console.log(`[SW] Ignore non GET request ${request.method}`);
    }
    return;
  }

  const requestUrl = new URL(request.url);

  // Ignore difference origin.
  if (requestUrl.origin !== location.origin) {
    if (DEBUG) {
      console.log(`[SW] Ignore difference origin ${requestUrl.origin}`);
    }
    return;
  }

  const resource = global.caches.match(request)
    .then((response) => {
      if (response) {
        if (DEBUG) {
          console.log(`[SW] fetch URL ${requestUrl.href} from cache`);
        }

        return response;
      }

      // Load and cache known assets.
      return fetch(request)
        .then((responseNetwork) => {
          if (!responseNetwork || !responseNetwork.ok) {
            if (DEBUG) {
              console.log(`[SW] URL [${
                requestUrl.toString()}] wrong responseNetwork: ${
                responseNetwork.status} ${responseNetwork.type}`);
            }

            return responseNetwork;
          }

          if (DEBUG) {
            console.log(`[SW] URL ${requestUrl.href} fetched`);
          }

          const responseCache = responseNetwork.clone();

          global.caches
            .open(CACHE_NAME)
            .then((cache) => {
              return cache.put(request, responseCache);
            })
            .then(() => {
              if (DEBUG) {
                console.log(`[SW] Cache asset: ${requestUrl.href}`);
              }
            });

          return responseNetwork;
        })
        .catch(() => {
          // User is landing on our page.
          if (event.request.mode === 'navigate') {
            return global.caches.match('./');
          }

          return null;
        });
    });

  event.respondWith(resource);
});

self.addEventListener('notificationclick', (event) => {
  // Android doesn't close the notification when you click it
  // See http://crbug.com/463146
  event.notification.close();
  const url = event.notification.data && event.notification.data.url;
  if (!url) {
    return;
  }

  // Check if there's already a tab open with this URL.
  // If yes: focus on the tab.
  // If no: open a tab with the URL.
  event.waitUntil(
    clients.matchAll({
      type: 'window',
    })
      .then((windowClients) => {
        for (let i = 0; i < windowClients.length; i++) {
          const client = windowClients[i];
          if (client.url === url && 'focus' in client) {
            return client.focus();
          }
        }
        if (clients.openWindow) {
          return clients.openWindow(url);
        }
      }),
  );
});

/**
 * Обрабатываем данные прищедшие с mqtt
 * @param event
 */
function handlerDataServiceWorker(event) {
  console.log('letpayload', event.notification);
  self.document.URL = event.data.documentUrl;
  const payload = JSON.parse(event.data.payload);

  const topic = 'private/chat';
  const notificationContent = getNotificationContent(payload);
  if (notificationContent && payload.createMessage.message.text != '$group') {
    showNotificationIfNoVisibleTabs(notificationContent);
  }

  sendMessageToAll({ action: 'mqttMessage', topic, payload });
}
/**
 * Отправляет сообщение всем открытым вкладкам
 *
 * @param data
 */
function sendMessageToAll(data) {
  self.clients.matchAll()
        .then(clients => { return Promise.all(clients.map(c => { return sendMessageToClient(c, data); })); });
}

/**
 * Отправляет сообщение в одну открутую вкладку
 *
 * @param client
 * @param data
 * @returns {Promise}
 */
function sendMessageToClient(client, data) {
  return new Promise((resolve, reject) => {
    const messageChannel = new MessageChannel();
    messageChannel.port1.onmessage = (event) => {
      if (event.data.error) {
        reject(event.data.error);
      } else {
        resolve(event.data);
      }
    };
    client.postMessage(data, [messageChannel.port2]);
  });
}

function getNotificationContent(payload) {
  if ((currentUser && currentUser.id == payload.createMessage.message.sender.id) || self.ids == payload.createMessage.message.id) {
    return;
  }
  self.ids = payload.createMessage.message.id;
  switch (payload.action) {
    case 'message_add':
      return {
        title: payload.threadName,
        body: payload.createMessage.message.text,
        data: {
          url: `${document.URL}/chat/thread/${payload.createMessage.message.recipient}`,
        },
      };
  }
}

function showNotificationIfNoVisibleTabs(notificationContent) {
  self.clients.matchAll()
        .then(clients => { return clients.filter(f => { return f.visibilityState == 'visible'; }); })
        .then(visibleClients => { return !visibleClients.length; })
        .then(showNotification => {
          if (!showNotification) return;
          return self.registration.showNotification(notificationContent.title, {
            body: notificationContent.body,
            icon: 'images/mipmap-xxhdpi/ic_launcher.png',
            data: notificationContent.data,
          });
        });
}
