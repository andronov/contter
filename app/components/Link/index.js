import * as React from 'react';
import { observer , inject} from 'mobx-react';

@inject("appState")
@observer
export default class Link extends React.Component {

  onClickLink = (e) => {
    let { href, appState, type} = this.props;
    e.preventDefault();
    console.log('onClickLink', e, href, appState, type);
    this.props.history.push(href);
    appState.color = appState.get_color(1);
    //appState.router.navigate(href);
    if(type){appState.app_content = type}
    if(type === 'profile'){
      appState.loadingProfile = true;
    }

  };

  render() {
    let { className, value, style, href} = this.props;

    return (
      <a href={href} onClick={(e) => this.onClickLink(e)} style={style} className={className}>{value}</a>
    );
  }
}