import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import ShareItem from '../MainPage/ShareItem';

@inject("appState")
@observer
export default class SharePage extends React.Component {

  onClickClose = () => {
    let {appState} = this.props;
    appState.update({shareItem: null, shareOpen: false});
    appState.router.navigate('/!/technology');
  };

  render() {
    const { user, appState} = this.props;
    let item = appState.shareItem;
    console.log('SharePage', appState.shareItem);

    return (
    <div style={{width: '100%', height: '100%'}}>
      <div onClick={this.onClickClose} className="share__close"/>

      <div className="share">
        {item ? <ShareItem item={item} share={true} user={appState.currentUser} /> : null}

      </div>

    </div>
    );
  }
}

/*
* <div className="share__block">
 <div className="share__inner">

 </div>
 </div>
* */