import * as React from 'react';
import { observer , inject} from 'mobx-react';
import Item from '../Item';
import './style.scss';
import TileGrid from '../Tile/index';
import { Scrollbars } from 'react-custom-scrollbars';
import LazyLoading from '../LazyLoading';
import * as _ from 'lodash';

//@inject("appState")
@observer
export default class FeedItem extends React.Component {

  renderLoading = () => {
    let loadingArrItems = _.range(10);
    loadingArrItems = loadingArrItems.map(it=>{
      return {
        id: it,
        height: _.random(200, 450),
        widthName: _.random(50, 98),
        widthUser: _.random(27, 67)
      }
    });
    return(
      <div className="feed__inner">
        <div className="feed__list">
          <LazyLoading className="lazy__feed">
            {loadingArrItems.map(item=>{
              return(
                <div key={item.id} style={{height: `${item.height}px`}} className="lazy__item">
                  <div className="lazy__item_inner">
                    <div style={{width: `${item.widthName}%`}} className="lazy__item_title">
                    </div>
                    <div className="lazy__item_footer">
                      <div className="lazy__item_footer_t">
                        <div style={{width: `${item.widthUser}px`}} className="lazy__item_footer_elem" />
                        <div style={{width: '35px'}} className="lazy__item_footer_elem" />
                        <div style={{width: '55px'}} className="lazy__item_footer_elem" />
                      </div>
                      <div className="lazy__item_footer_r">
                        <div style={{width: '35px'}} className="lazy__item_footer_elem" />
                        <div style={{width: '30px'}} className="lazy__item_footer_elem" />
                      </div>
                    </div>
                  </div>
                </div>
              )
            })}
          </LazyLoading>
        </div>
      </div>
    )
  };

  handleScroll = (e) => {
    let { appState, user } = this.props;
    let diff = e.currentTarget.scrollHeight - e.currentTarget.scrollTop;
    let { activeWall } = user;

    //TODO высчитывать три последних
    //TODO курсор не должен прыгать
    if(diff < 1000 && !appState.loadingGlobalItems){
      ///console.log('handleScroll', diff, activeWall.lastOffset, activeWall.lastLimit);
      appState.loadingGlobalItems = true;
      user.loadItems(activeWall.slug,
        activeWall.lastOffset+activeWall.lastLimit,
        activeWall.lastLimit, [], activeWall);
    }
  };

  render() {
    let { user, items, noscroll, appState } = this.props;
    //let loadingGlobalItems = false;
    let { loadingGlobalItems } = appState;
    let activeWall = user ? user.activeWall : null;
   // console.log('FeedItem', noscroll, this);
    let loadingItems = activeWall && activeWall.loadingItems;

    let isTile = false;
    if(isTile){
      items = items.filter(it => it.json_img).map(item => {
        let res = item.arcImage();
        console.log('ARG9', res);
        return {
          id: item.id,
          title: item.title,
          medium_img: 'https://s3-us-west-2.amazonaws.com/contter/'+item.medium_img,
          json_img: {
            width: res.width,//item.json_img.medium_img.width,
            height: res.height//item.json_img.medium_img.height
          },
          created: item.created,
          href: item.link,
          item: item
        }
      });
      return(
        <Scrollbars className='feed__scroll' style={{ width: '100%', height: '100%'}}>
          <div style={{margin: '0 auto', width: '100%', position: 'relative'}}><TileGrid items={items}/></div>
        </Scrollbars>
      )
    }

    return (
      <div style={{height: '100%'}} className="feed">
        {!noscroll ? <Scrollbars onScroll={this.handleScroll} className='feed__scroll' style={{ width: '100%', height: '100%'}}>
          <div className="feed__inner">
            <div className="feed__list">
              {items.map(item=> <Item user={user} item={item} {...this.props}/>)}
              {(loadingItems && !activeWall.loadedItems) || loadingGlobalItems ? this.renderLoading() : null}
            </div>
          </div>
        </Scrollbars> : null}
        {noscroll ?
          <div className="feed__inner">
            <div className="feed__list">
              {items.map(item=> <Item user={user} item={item} {...this.props}/>)}
            </div>
          </div>: null}
        {/*<Scrollbars className='feed__scroll' style={{ width: '100%', height: '100%'}}>
          <div style={{margin: '0 auto', width: '100%', position: 'relative'}}><TileGrid items={items}/></div>

        </Scrollbars>*/}
      </div>
    );
  }
}
/*
* {
 loadingItems ? <LazyLoading className="lazy__feed">l2oad</LazyLoading> :
 items.map(item=> <Item user={user} item={item} {...this.props}/>)
 }
* */