import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';

@inject("appState")
@observer
export default class Palette extends React.Component {

  render() {
    const { user, appState, onClickItem, colorItem } = this.props;
    let color = appState.get_color(1, null, colorItem);
    let colorOpacity = appState.get_color('0.20', null, colorItem);
    let backOpacity = appState.get_color( '0.07', null, colorItem);
    console.log(' keys',  color);
    let colors = appState.colors;

    return (
      <div className='palette'>
        {colors.map((item, i) => {
          return (<div key={i} onClick={(it) => onClickItem(item)} className={colorItem == item.color ? "palette__item active" : "palette__item"} style={{background: '#'+item.color}}></div>)
        })}
      </div>
    );
  }
}