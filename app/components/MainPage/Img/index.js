import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';

@inject("appState")
@observer
export default class Img extends React.Component {

  onLoadImage = (e) => {
    console.dir(e.target);
    console.log('onLoadImage', e.target);
  };
  onErrorImage = (e) => {
    let { type } = this.props;
    console.log('onErrorImage', e);
    console.dir(e.target);
    // TODO Сделать дефолтные оишбки для картинки
    if(type === 'wall_search'){
      e.target.src = '/favicon.ico'
    }
    if(type === 'wall_source'){
      e.target.src = '/favicon.ico'
    }
    if(type === 'profile'){
      e.target.src = '/favicon.ico'
    }
  };

  render() {
    let { src, style } = this.props;

    return (
      <img style={style} onError={this.onErrorImage} onLoad={this.onLoadImage} src={src} />
    );
  }
}