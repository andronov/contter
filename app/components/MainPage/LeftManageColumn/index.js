import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import Sortable from 'react-anything-sortable';
import { sortable } from 'react-anything-sortable';

// style={{float: 'none'}}

@observer
@sortable
class WallSortItem extends React.Component {
  render() {
    return (
      <div {...this.props} >
        {this.props.children}
      </div>
    );
  }
}

@inject("appState")
@observer
export default class LeftManageColumn extends React.Component {


  renderWalls = () => {
    let { items, handleSort, onClickMenuItem, editItem, color, editWallFull, onClickMenuItemSub} = this.props;
    return(
      <Sortable onSort={handleSort} sortHandle="handleWall" className="vertical-container" direction="vertical" dynamic>
      {items.map((item, i) => {
        item.editItem = editItem;
        return (
          <WallSortItem className="vertical vertical__wall" sortData={item} key={item.sort}>
            <div className="handleWall"><div></div></div>
          <div className={"lf_column__item hovered " + (item.editItem && item.editItem.id === item.id ? ' active ' : '')
          + (editItem && editItem.id === item.id && editWallFull ? ' active-bottom' : '')}>
            <div onClick={(it) => onClickMenuItem(item)}
                 className="lf_column__item_inner">
              <h2
                className="lf_column__item__title">{item.name}</h2>
              <p className="lf_column__item__desc">
                /{item.slug}</p>
              <span className="lf_column__line"
                    style={{background: color}} />
            </div>

            {editItem && editItem.id === item.id ?
              <div
                onClick={(it) => onClickMenuItemSub(item)}
                className={"lf_column__item hovered_sub lf_column__item_sub " + (editWallFull ? ' active-top' : '')}>
                <div className="lf_column__item_inner">
                  <h2 className="lf_column__item__title">
                    Edit</h2>
                  <p className="lf_column__item__desc">Info,
                    filter and etc.</p>
                  <span className="lf_column__line"
                        style={{background: color}}/>
                </div>
              </div>
              : null}
          </div>
          </WallSortItem>
        )
      })}
        </Sortable>
    )
    //style="background: rgb(0, 136, 122);"
  };

  renderHeader = () => {
    let { addeditwall, followers, color,
      backOpacity, colorOpacity, user } = this.props;
    if(addeditwall){
      return (
        <div style={{backgroundColor: backOpacity,
          borderColor:  colorOpacity,
          borderBottomWidth: '1px',
          borderBottomStyle: 'solid'}} className="lf_column__item lf_column__item__header">
          <div className="lf_column__item_inner">
            <h2 className="lf_column__item__title">Your walls</h2>
            <p className="lf_column__item__desc">By interests and type</p>
          </div>
        </div>
      )
    }
    if(followers){
      return (
        <div style={{backgroundColor: backOpacity,
          borderColor:  colorOpacity,
          borderBottomWidth: '1px',
          borderBottomStyle: 'solid'}} className="lf_column__item no-cursor lf_column__item__header">
          <div className="lf_column__item_inner no-cursor">
            <h2 className="lf_column__item__title">{user.username}</h2>
            <p className="lf_column__item__desc">{user.first_name}</p>
          </div>
        </div>
      )
    }
  };

  renderFooter = () => {
    let { addeditwall, onClickMenuAddWall,
      activeFooter, color, backOpacity,
      colorOpacity  } = this.props;
    if(addeditwall){
      return (

        <div onClick={onClickMenuAddWall} style={{borderColor:  colorOpacity,
          borderTopWidth: '1px',
          borderTopStyle: 'solid'}}  className={"lf_column__item no-select hovered lf_column__item__footer " + (activeFooter ? 'active' : '')}>
          <div className="lf_column__item_inner">
            <h2 className="lf_column__item__title">Create new wall</h2>
            <p className="lf_column__item__desc">Personalized the Internet</p>
            <span className="lf_column__line" style={{background: color}}></span>
          </div>
        </div>
      )
    }
  };

  renderFollowers = () => {
    let { addeditwall, followers, color,
      backOpacity, colorOpacity, user } = this.props;
    return (

      <div className="lf_column__item no-cursor active">
        <div  className="lf_column__item_inner no-cursor">
          <h2 className="lf_column__item__title">Followers</h2>
          <p className="lf_column__item__desc">{user.followersCount}</p>
          <span className="lf_column__line" style={{background: color}}></span>
        </div>
      </div>
    )
  };

  render() {
    const { user, addeditwall, color, followers,
      backOpacity, colorOpacity} = this.props;
   // console.log('LeftManageColumn', this);
    let templateItems = null;
    if(addeditwall){
      templateItems = this.renderWalls()
    }
    if(followers){
      templateItems = this.renderFollowers()
    }
    let templateHeader = this.renderHeader();
    let templateFooter = this.renderFooter();

    return (
      <div className="lf_column" style={{borderColor: colorOpacity}}>
        {templateHeader}
        {templateItems}
        {templateFooter}
      </div>
    );
  }
}