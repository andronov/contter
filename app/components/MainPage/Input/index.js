import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';

@inject("appState")
@observer
export default class Input extends React.Component {

  render() {
    const { user, children, appState,
      styleInput, showline, colorItem,
      onChangeInput, keys, value,
      placeholder } = this.props;
    let color = appState.get_color(1, null, colorItem);
    let colorOpacity = appState.get_color('0.20', null, colorItem);
    let backOpacity = appState.get_color( '0.07', null, colorItem);

    return (
      <div style={styleInput} className='cn_input'>
        <input onChange={(e, k) => onChangeInput(e.target.value, keys)} key={keys} value={value} style={{'borderColor':colorOpacity}} className='cn_input__field' placeholder={placeholder}/>
        {children}
        {showline ?
          <span  className='cn_input__line'>
            <span style={{'background':color}}></span>
          </span>
          : null}
      </div>
    );
  }
}