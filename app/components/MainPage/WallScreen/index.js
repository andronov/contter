import * as React from 'react';
import { observer , inject} from 'mobx-react';
import Screen from '../Screen';
import Header from '../Header';
import FeedScreen from '../FeedScreen';
import ProfilePage from '../../MainPage/ProfilePage';

@inject("appState")
@observer
export default class WallScreen extends React.Component {
  componentWillMount() {
    console.log('maIN', this.props.matches);
  };

  getUser = () => {
    let { appState } = this.props;
    let username = this.props.match.params.username;
    if(this.props.match.params.slug){
      username += '/' + this.props.match.params.slug;
    }
    let us = appState.users.filter(us => us.username === username);
    return us.length && us[0]
  };

  componentDidMount = () => {
    // TODO сделать красиво и куда то перенести ?
    let { appState } = this.props;
    let profileUser = this.getUser();
    if(!profileUser){
      appState.getUser(this.props.match.params.username + (
          this.props.match.params.slug ? '/'+this.props.match.params.slug: ''
        )
      );
      return
    }
    profileUser.loadItems('#', 0, 10);
    if(appState.currentUser !== profileUser){
      profileUser.loadAllUserWalls()
    }
  };

  render() {
    const { appState } = this.props;
    let user = appState.currentUser;
    window.matchRoute = this.props.match;
    window.locRoute = this.props.location;
    let alien = false;
    let profileUser = this.getUser();
    if(profileUser &&  profileUser !== user)alien = true;
    if(!alien) {
      return (
        <Screen
          header={<Header user={user} {...this.props}/>}
          children={<FeedScreen
            user={user} {...this.props}/>}>
        </Screen>
      );
    }
    else{
      return(<ProfilePage {...this.props}/>)
    }
  }
}
