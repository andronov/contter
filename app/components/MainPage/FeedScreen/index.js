import * as React from 'react';
import { observer , inject} from 'mobx-react';
import FeedItem from '../FeedItem';

@inject("appState")
@observer
export default class FeedScreen extends React.Component {

  render() {
    let { appState } = this.props;
    let user = appState.currentUser;

    if(!user) return(<div></div>);

    let wall = user.activeWall;
    let items = wall ? wall.items : [];
    //console.log('FeedScreen', wall, items);
    return (
      <div style={{height: '100%'}}>
        <FeedItem items={items} user={user} {...this.props}/>
      </div>
    );
  }
}
