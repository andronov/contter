import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import Screen from '../Screen';
import Header from '../Header';
import ManageWall from './ManageWall';
import { Scrollbars } from 'react-custom-scrollbars';

@inject("appState")
@observer
export default class AddEditWall extends React.Component {

  render() {
    const { user } = this.props;
    console.log('wallScreen', user && user.id);

    return (
      <Screen header={<Header user={user} {...this.props}/>} children={
        <Scrollbars className='' style={{ width: '100%', height: '100%'}}>
          <div className="cn-container">
            <ManageWall user={user} {...this.props}/>
          </div>
        </Scrollbars>
      }>
      </Screen>
    );
  }
}