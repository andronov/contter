import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import LeftManageColumn from '../../LeftManageColumn';
import Input from '../../Input';
import Palette from '../../Palette';
import { Scrollbars } from 'react-custom-scrollbars';
import Link from '../../../Link';
import Img from '../../Img';
import Loader from '../../Loader';
import Switcher from '../../Switcher';


@inject("appState")
@observer
export default class ManageWall extends React.Component {

  state = {
    editWall: null,
    editWallName: '',
    editWallSlug: '',
    editWallColor: '',
    editWallExcluding: '',
    editWallMatching: '',
    editWallArrExcluding: [],
    editWallArrMatching: [],
    editWallFull: false,
    wallName: '',
    colorItem: '',
    wallMatching: '',
    wallType: 1,
    wallExcluding: '',
    wallArrMatching: [],
    wallArrExcluding: [],
    showWordsHint: false,
    showDeletePopover: false,
    saving: false,
    showWordsType: 'matching',
    searchSourceText: ''
  };

  componentDidUpdate = (prevProps, prevState) => {
   // console.log('componentDidUpdate', prevProps, prevState);
  };

  onClickMenuAddWall = () => {
    let { appState } = this.props;

    console.log('onClickMenuAddWall');
    this.props.history.push(`/add`);
    this.setState({editWall: null});
  };

  onClickMenuItem = (item) => {
    let { appState } = this.props;
    console.log('onClickMenuItem ', item);
    this.props.history.push(`/add#${item.slug}`);
    appState.searchWallSources = [];
    item.getWallSources();
    this.setState({editWall: item, editWallFull: false,
      editWallName: item.name, editWallSlug: item.slug,
      editWallColor: item.color,
      colorItem: item.color,
      searchSourceText: '',
      editWallMatching: item.words.matching.join(', '),
      editWallExcluding: item.words.excluding.join(', ')});
  };

  onClickMenuItemSub = (item) => {
    let { appState } = this.props;
    console.log('onClickMenuItemSub', item);
    this.setState({editWallFull: true});
  };

  textChangedEdit = (value, key) => {
    console.log('textChangedEdit', value, key);
    let d = {};
    d[key] = value;
    if(value.length > 20){
      return
    }
    if((!value.replace('/', '').match(/^[a-zA-Z0-9]*$/) &&
      (key == 'slug'))){
      return
    }
    /*if((!value.replace('/', '').match(/^[a-zA-Z0-9]*$/) &&
      (key == 'slug' || key == 'wallName' || key == 'name'))
      || value.length > 20){
      return
    }*/
    if(key == 'slug' || key == 'wallName' || key == 'name'){
      value = value.replace('/', '')
    }
    this.setState(d)
  };

  textChangedWallEdit = (value, key) => {
    console.log('textChangedWallEdit', value, key);
    let d = {};

    if((!value.replace('/', '').match(/^[a-zA-Z0-9]*$/) &&
      (key == 'editWallSlug' || key == 'slug' || key == 'wallName' || key == 'name'))
      && value.length > 20){
      return
    }
    if((!value.replace('/', '').match(/^[a-zA-Z0-9]*$/) && key == 'editWallSlug')){
      return
    }
    if(key == 'editWallSlug' || key == 'slug' || key == 'wallName' || key == 'name'){
      value = value.replace('/', '')
    }
    d[key] = value;
    this.setState(d)
  };

  searchSource = (value, key) => {
    let { appState } = this.props;
    console.log('searchSource', value, key);
    let d = {};
    d[key] = value;
    this.setState(d);
    appState.getAddWallSearch(value);
  };

  onClickItem = (item) => {
    console.log('onClickItem', item);
    this.setState({colorItem: item.color})
  };

  onClickEditColorItem = (item) => {
    this.setState({editWallColor: item.color,
      colorItem: item.color})
  };

  onMouseOverSearchSource = (e) => {
    let { appState} = this.props;
    let { colorItem } = this.state;
    let color = appState.get_color(1, null, colorItem);
    let colorOpacity = appState.get_color('0.20', null, colorItem);
    e.target.style.backgroundColor = color;
  };

  onMouseOutSearchSource = (e) => {
    e.target.style.backgroundColor = null;
  };

  onClickSearchSource = (e, item) => {
    let { editWall } = this.state;
    let check = true;
    if(item.type === 'SITE'){
      check = editWall.originalSources.sites.indexOf(item.id) === -1;
    }
    else{
      check = editWall.originalSources.users.indexOf(item.id) === -1;
    }
    editWall.editWallSource(item, check);
  };

  renderSearchWallSources = () => {
    let { appState} = this.props;
    let { colorItem, editWall} = this.state;
    let color = appState.get_color(1, null, colorItem);
    let colorOpacity = appState.get_color('0.20', null, colorItem);
    let lists = appState.searchWallSources;

    return (
      <div className="m_wall__search_sources">
        <Scrollbars style={{ width: '100%', height: lists.length*50 > 200 ? '200px' : lists.length*50+'px'}}>
          {lists.map((item, i) => {
            if(item.type === 'SITE'){
              let site_check = editWall.originalSources.sites.indexOf(item.id) != -1;
              return (
                <div key={"site_"+item.id} className="m_wall__search_source">
                  <div className="m_wall__search_source_info">
                    <div>
                      {item.name} <Link type="profile" style={{color: color, textDecoration: 'none'}}
                                        href={`/${item.slug}`} value={item.slug} to={`/${item.slug}`}/>
                    </div>
                    <div>
                      {item.description}
                    </div>
                  </div>
                  <div className="m_wall__search_source_btn">
                      <div onClick={(e, it) => this.onClickSearchSource(e, item)} onMouseOut={this.onMouseOutSearchSource} onMouseOver={this.onMouseOverSearchSource} style={{borderColor: colorOpacity}}
                           className={"m_wall__search_source_btn_m " + (site_check ? ' delete ' : ' add ')}>
                      </div>
                  </div>
                  <div className="m_wall__search_source_img">
                    <Img type="wall_search" src={`https://s3-us-west-2.amazonaws.com/contter/${item.favicon}`}/>
                  </div>
                </div>
              )
            }
            else{
              let user_check = editWall.originalSources.users.indexOf(item.id) != -1;
              return (
                <div key={"user_"+item.id} className="m_wall__search_source">
                  <div className="m_wall__search_source_info">
                    <div>
                      {item.first_name} <Link type="profile" style={{color: color, textDecoration: 'none'}}
                                        href={`/${item.username}`} value={item.username} to={`/${item.username}`}/>
                    </div>
                    <div>
                      {item.description}
                    </div>
                  </div>
                  <div className="m_wall__search_source_btn">
                    <div onClick={(e, it) => this.onClickSearchSource(e, item)} onMouseOut={this.onMouseOutSearchSource} onMouseOver={this.onMouseOverSearchSource} style={{borderColor: colorOpacity}}
                         className={"m_wall__search_source_btn_m " + (user_check ? ' delete ' : ' add ')}>
                    </div>
                  </div>
                  <div className="m_wall__search_source_img">
                    <Img type="wall_search" src={`https://s3-us-west-2.amazonaws.com/contter/${item.avatar}`}/>
                  </div>
                </div>
              )
            }
          })}
        </Scrollbars>
      </div>
    )
  };

  renderWordsHint = (type) => {
    let { appState } = this.props;
    let { colorItem} = this.state;
    let color = appState.get_color('0.80', null, colorItem);
    return(
      <div onClick={(tp) => this.onClickWordsHint(type)} style={{backgroundColor: color}} className="m_wall__hint_words">
        <div className="m_wall__hint_words_icon"></div>
        <div className="m_wall__hint_words_text">select from collections</div>
      </div>
    )
  };

  onClickWordsHint = (type) => {
    console.log('onClickWordsHint', type);
    this.setState({showWordsHint: true,
      showWordsType: type});
  };

  onClickWallSource = (e, item) => {
    console.log('onClickWallSource', e, item);
    let { editWall } = this.state;
    let check = true;
    if(item.type === 'SITE'){
      check = editWall.originalSources.sites.indexOf(item.id) === -1;
    }
    else{
      check = editWall.originalSources.users.indexOf(item.id) === -1;
    }
    editWall.editWallSource(item, check);
  };

  renderListSources = () => {
    let { appState } = this.props;
    let { editWall, colorItem } = this.state;
    let lists = editWall && editWall.listSources;
    let color = appState.get_color(1, null, colorItem);
    let colorOpacity = appState.get_color('0.20', null, colorItem);
    let backOpacity = appState.get_color( '0.07', null, colorItem);


    return (
      <div className="m_wall__source">
        {lists.map((item, i) => {
          if(item.type === 'SITE') {
            let site_check = editWall.originalSources.sites.indexOf(item.id) != -1;
            return (
              <div key={i}
                   style={{borderColor: colorOpacity}}
                   className="m_wall__source_item">
                <div className="m_wall__source_item_top">
                  <div style={{color: color}} className="m_wall__source_item_title">{item.name}</div>
                  <div className="m_wall__source_item_username">
                    <Link type="profile" style={{color: '#000', textDecoration: 'none'}}
                          href={`/${item.slug}`} value={'/'+item.slug} to={`/${item.slug}`}/>
                  </div>
                </div>

                <div className="m_wall__source_item_avatar">
                  <Img type="wall_source" src={`https://s3-us-west-2.amazonaws.com/contter/${item.image}`} style={{
                    width: '108px',
                    marginLeft: '-1px'
                  }}/>
                </div>

                <div className="m_wall__source_item_footer">
                  <div onClick={(e, it) => this.onClickWallSource(e, item)} onMouseOut={this.onMouseOutSearchSource} onMouseOver={this.onMouseOverSearchSource} style={{borderColor: colorOpacity}}
                       className={"m_wall__source_item_footer_btn " + (site_check ? ' delete ' : ' add ')}>
                  </div>
                </div>

              </div>
            )
          }
          else{
            let user_check = editWall.originalSources.users.indexOf(item.id) != -1;
            return (
              <div key={i}
                   style={{borderColor: colorOpacity}}
                   className="m_wall__source_item">
                <div className="m_wall__source_item_top">
                  <div style={{color: color}} className="m_wall__source_item_title">{item.first_name}</div>
                  <div className="m_wall__source_item_username">
                    <Link type="profile" style={{color: '#000', textDecoration: 'none'}}
                          href={`/${item.username}`} value={'/'+item.username} to={`/${item.username}`}/>
                  </div>
                </div>

                <div className="m_wall__source_item_avatar">
                  <Img type="wall_source" src={`https://s3-us-west-2.amazonaws.com/contter/${item.avatar}`} style={{
                    width: '108px',
                    marginLeft: '-1px'
                  }}/>
                </div>

                <div className="m_wall__source_item_footer">
                  <div onClick={(e, it) => this.onClickWallSource(e, item)} onMouseOut={this.onMouseOutSearchSource} onMouseOver={this.onMouseOverSearchSource} style={{borderColor: colorOpacity}}
                       className={"m_wall__source_item_footer_btn " + (user_check ? ' delete ' : ' add ')}>
                  </div>
                </div>

              </div>
            )
          }
        })}
      </div>
    );
  };

  onClickCloseWordsHint = () => {
    this.setState({showWordsHint: false,
      showWordsType: 'matching'});
  };

  onClickCreate = () => {
    const { appState } = this.props;
    let user = appState.currentUser;
    let that = this;
    appState.savingAddWall = true;
    // TODO Сделать валидатор
    /* let wallFollowing = this.state.wallFollowing.map((it, i) => {
     return {id: it.id, type: it.type}
     })*/
    let data  = {
      wallName: this.state.wallName,
      wallColor: this.state.colorItem,
      wallExFilter: this.state.wallArrExcluding,
      wallMtFilter: this.state.wallArrMatching,
      wallWordExFilter: this.state.wallExcluding,
      wallWordMtFilter: this.state.wallMatching
    };

    user.addWall(data).then(wall => {
      console.log('addWall2', wall);
      that.onClickMenuItem(wall)
    }).catch(error => {
      console.log('error', error)
    });


    //HttpApi.addWall
    console.log('onClickCreate', data);
  };

  onClickDelete = () => {
    console.log('onClickDelete');
    this.state.editWall.delete();
    this.props.history.push(`/add`);
    this.setState({editWall: null});
  };

  onClickSaveChanges = () => {
    console.log('onClickSaveChanges');
    const { appState } = this.props;

    let user = appState.currentUser;
    let that = this;
    appState.savingEditWall = true;

    let data  = {
      id: this.state.editWall.id,
      name: this.state.editWallName,
      slug: this.state.editWallSlug,
      color: this.state.editWallColor
    };
    /*
     editWallExcluding: '',
     editWallMatching: '',
     editWallArrExcluding: [],
     editWallArrMatching: [],
    * */

    data['wallExFilter'] =  this.state.editWallArrExcluding;
    data['wallMtFilter'] =  this.state.editWallArrMatching;
    data['wallWordExFilter'] =  this.state.editWallExcluding;
    data['wallWordMtFilter'] =  this.state.editWallMatching;
    this.state.editWall.updateWall(data)
  };

  onChangeSwitcher = (action, type) => {
    console.log('onChangeSwitcher', action, type);
    if(type === 'wallType'){
      this.setState({wallType: action ? 1 : 2})
    }
  };

  handleSort = (items) => {
    let { appState } = this.props;
    let user = appState.currentUser;
    user.setSortWalls(items.map((it, i)=>{
      return{id: it.id, sort: i}}));
    console.log('handleSort', items);
    items.forEach((item, i) => {
      item.sort = i
    });
  };

  render() {
    const { appState } = this.props;
    let user = appState.currentUser;
    let saving = appState.savingAddWall;
    let savingChanges = appState.savingEditWall;
    let { wallName, colorItem,
      wallExcluding, wallMatching,
      editWall, editWallFull, searchSourceText,
      editWallName, editWallColor, editWallSlug,
      editWallExcluding, editWallMatching,
      wallType,
      showWordsHint, showDeletePopover } = this.state;
    //console.log('ManagedfgdfdWall', appState.get_color());

    let color = appState.get_color(1, null, colorItem);
    let colorOpacity = appState.get_color('0.20', null, colorItem);
    let backOpacity = appState.get_color( '0.07', null, colorItem);
    let items = user.walls.sort((a,b)=> a.sort - b.sort);
    //console.log('.listSources', editWall && editWall.listSources);

    return (
      <div className="cn-container-inner">
        <div style={{minWidth: '190px'}} className="cn-container-left">
          <LeftManageColumn activeFooter={!editWall} color={color} colorOpacity={colorOpacity}
                            backOpacity={backOpacity}
                            editItem={editWall}
                            handleSort={this.handleSort}
                            editWallFull={editWallFull}
                            onClickMenuItemSub={this.onClickMenuItemSub}
                            onClickMenuItem={this.onClickMenuItem}
                            onClickMenuAddWall={this.onClickMenuAddWall}
                            addeditwall={true} items={items}/>
        </div>
        <div style={{width: '100%'}} className="cn-container-right">
          <div className="m_wall">
            {showWordsHint ?
              <div className="m_wall__inner">
                <div onClick={this.onClickCloseWordsHint} className="m_wall__w_hint_close">x</div>
              </div>
              : null}
            {!editWall && !showWordsHint ? <div className="m_wall__inner">
              <div>
                <Input colorItem={colorItem} styleInput={{marginTop: '0'}} showline={true} onChangeInput={this.textChangedEdit} keys={'wallName'} value={wallName} placeholder='Name' />
              </div>
              <div>
                <Palette colorItem={colorItem} onClickItem={this.onClickItem} />
              </div>
              <div>
                <Input children={this.renderWordsHint('matching')} colorItem={colorItem} showline={true} onChangeInput={this.textChangedEdit} keys={'wallMatching'} value={wallMatching} placeholder='Enter words to match' />
              </div>
              <div>
                <Input children={this.renderWordsHint('excluding')} colorItem={colorItem} styleInput={{marginTop: '0'}} showline={true} onChangeInput={this.textChangedEdit} keys={'wallExcluding'} value={wallExcluding} placeholder='Enter words to match excluding' />
              </div>
              <div style={{display: 'none'}}>
                <div className="m_wall__mode">
                  <Switcher {...this.props}
                    active={wallType === 1}
                    type="wallType"
                    onChangeSwitcher={this.onChangeSwitcher}
                    labelText='Public wall, Anyone see news'
                  />
                </div>
              </div>
              <div style={{display: 'none'}}>
                <div className="m_wall__sentiment">
                  <div className="m_wall__sentiment_field">
                    <Switcher {...this.props}
                              active={wallType === 1}
                              type="wallType"
                              onChangeSwitcher={this.onChangeSwitcher}
                              labelText='Positive news'
                    />
                    </div>
                  <div className="m_wall__sentiment_field">
                    <Switcher {...this.props}
                              active={wallType === 1}
                              type="wallType"
                              onChangeSwitcher={this.onChangeSwitcher}
                              labelText='Neutral news'
                    />
                    </div>
                  <div className="m_wall__sentiment_field">
                    <Switcher {...this.props}
                              active={wallType === 1}
                              type="wallType"
                              onChangeSwitcher={this.onChangeSwitcher}
                              labelText='Negative news'
                    />
                    </div>

                </div>

              </div>
              <div className="m_wall__footer_bl">
                <div></div>
                <div style={{color: color}} onClick={this.onClickCreate} className="m_wall__create">
                  {!saving ? 'CREATE' : <Loader style={{borderWidth: '3px', width: '15px', height: '15px'}} color={color} />}
                </div>
              </div>
            </div> : null}
            {editWall && !editWallFull ? <div className="m_wall__inner">
              <div>
                <Input colorItem={colorItem} styleInput={{marginTop: '0'}} showline={true} onChangeInput={this.searchSource} keys={'searchSourceText'} value={searchSourceText} placeholder='Start typing any name or word to search' />
              </div>
              <div>
                {this.renderSearchWallSources()}
              </div>
              <div>
                {this.renderListSources()}
              </div>
              </div> : null}
            {editWall && editWallFull ? <div className="m_wall__inner">
              <div>
                <Input colorItem={colorItem} styleInput={{marginTop: '0'}} showline={true} onChangeInput={this.textChangedWallEdit} keys={'editWallName'} value={editWallName} placeholder='Name' />
              </div>
              <div>
                <Input colorItem={colorItem} styleInput={{marginTop: '0'}} showline={true} onChangeInput={this.textChangedWallEdit} keys={'editWallSlug'} value={'/'+editWallSlug} placeholder='Url' />
              </div>
              <div>
                <Palette colorItem={editWallColor} onClickItem={this.onClickEditColorItem} />
              </div>
              <div>
                <Input colorItem={colorItem} showline={true} onChangeInput={this.textChangedWallEdit} keys={'editWallMatching'} value={editWallMatching} placeholder='Enter words to match' />
              </div>
              <div>
                <Input colorItem={colorItem} styleInput={{marginTop: '0'}} showline={true} onChangeInput={this.textChangedWallEdit} keys={'editWallExcluding'} value={editWallExcluding} placeholder='Enter words to match' />
              </div>
              <div className="m_wall__footer_bl">
                <div  className="m_wall__delete">
                  <div className="m_wall__tooltip">
                    <div className="m_wall__tooltip_title">
                      Are you sure?
                    </div>
                    <div onClick={this.onClickDelete} style={{background: color}} className="m_wall__tooltip_btn">
                      Yes
                    </div>
                  </div>
                  DELETE
                </div>
                <div onClick={this.onClickSaveChanges} style={{color: color}} className="m_wall__create">
                  {!savingChanges ? 'SAVE CHANGES' : <Loader style={{borderWidth: '3px', width: '15px', height: '15px'}} color={color} />}
                  </div>
              </div>
            </div> : null}
          </div>
        </div>
      </div>
    );
  }
}