import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import HeaderMenu from './HeaderMenu';
import Link from '../../Link';
import LazyLoading from '../LazyLoading';

@inject("appState")
@observer
export default class Header extends React.Component {
  render() {
    let { appState} = this.props;
    let user = appState.currentUser;
    let color = appState.color ? appState.color : appState.get_color(1);
    if(!user) return(
      <LazyLoading >
        <header style={{backgroundColor: color}} className="header" />
      </LazyLoading>
    );

    //console.log('Header', user);
    //let walls = user.walls.sort((a,b)=> a.sort - b.sort);

    let top_menus = [
      {name: "#top"},
      {name: "#funny"},
      {name: "#news"},
      {name: "#gif"},
      {name: "#gaming"},
      {name: "#putin"}
      ];

    return (
      <header style={{backgroundColor: color}} className="header">
        <div className="header__row header__row_top">
          <div style={{width: '50px'}} >
          </div>
          <div className="header__top_menu">
            {top_menus.map(item => {
              return(
                <div key={item.name} className="header__top_menu_item">{item.name}</div>
              )
            })}
          </div>
          <div style={{width: '50px'}} className="header__profile">
            <Link type="profile" style={{color: 'white', textDecoration: 'none'}}
                  href={`/${user.username}`}
                  value={<div className="header__profile_icon"></div>}
                  to={`/${user.username}`} {...this.props}/>
          </div>
        </div>
        <div className="header__row header__row_footer">
          <div style={{width: '50px'}} className="header__column_wall">
            {/*<Link type="column" style={{color: 'white', textDecoration: 'none'}}
                  href="/column" value={<div className="header__column_wall_icon"></div>} to="/column"/>*/}
          </div>
          <HeaderMenu user={user} {...this.props}/>
          <div style={{width: '50px'}} className="header__add_wall">
            <Link type="add_wall" style={{color: 'white', textDecoration: 'none'}}
                  href="/add" value={<div className="header__add_wall_icon"></div>} to="/add" {...this.props}/>
          </div>
        </div>
      </header>
    );
  }
}