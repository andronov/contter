import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import Tabs from './Tabs';
import { Scrollbars } from 'react-custom-scrollbars';

@inject("appState")
@observer
export default class HeaderMenu extends React.Component {

  onClickTab = (i) => {
    let { appState} = this.props;
    let user = appState.currentUser;
    let w = user.walls.sort((a,b)=> a.sort - b.sort)
      .filter((wl, z) => z === i)[0];

    if(!w.loadItems){w.loadingItems = true}
    user.loadItems(w.slug, 0, 10, [], w);

    this.props.history.push(`/${user.username}/!/${w.slug}`);
    appState.color = appState.get_color(1, null, w.color);
    user.activeWall = w;
  };

  render() {
    let { appState } = this.props;

    //console.log('mt', window.matchRoute, window.locRoute);

    let user = appState.currentUser;
    if(!user) return(<div></div>);
    let slugwall = window.matchRoute ? window.matchRoute.params.slugwall: '';
    let walls = user.walls.sort((a,b)=> a.sort - b.sort);
    let symbols = 0;
    let selected = 0;

    let tabs = walls.map((wall, i) => {
      symbols += wall.name ? wall.name.length : 0;
      if(slugwall.toLowerCase() === wall.slug.toLowerCase())selected = i;
      return {displayName: wall.name, key: wall.id, new_item: wall.new_item}
    });

    //style={{width: (symbols*54) + 'px'}}
    return (
      <div className="header__menu">
        <Scrollbars className='header__menu__scroll' style={{ width: '100%', height: '46px'}}>
          <div >
            <Tabs onTabChange={this.onClickTab} selected={selected} tabs={tabs} />
          </div>
        </Scrollbars>
      </div>
    );
  }
}