import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import Img from '../Img';
import Link from '../../Link';

@inject("appState")
@observer
export default class MiniCard extends React.Component {

  componentDidMount = () => {
  };

  render() {
    const { user, appState, item, image,
      slug, type, title } = this.props;
    let color = appState.get_color(1, null, user.color);
    let colorOpacity = appState.get_color('0.20', null, user.color);
    let backOpacity = appState.get_color( '0.07', null, user.color);

    console.log('color', color, user.color);
    return (
      <div style={{borderColor: colorOpacity}} className="mini_card">

        <div className="m_wall__source_item_top">
          <div style={{color: color}} className="m_wall__source_item_title">{title}</div>
          <div className="m_wall__source_item_username">
            <Link type={type} style={{color: '#000', textDecoration: 'none'}}
                  href={`/${slug}`} value={'/'+slug} to={`/${slug}`} {...this.props}/>
          </div>
        </div>

        <div className="m_wall__source_item_avatar">
          <Img type="wall_source" src={image} style={{
            width: '108px',
            marginLeft: '-1px'
          }}/>
        </div>

      </div>
    );
  }
}