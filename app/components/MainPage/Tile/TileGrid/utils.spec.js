/* eslint global-require:0 */
/* eslint import/no-dynamic-require:0 */
import chai, { should } from 'chai';
import chaiEnzyme from 'chai-enzyme';

import {
  normalize,
  revertSortByCreated,
  Item,
  VerticalItem,
  createItem,
  Row,
  getTiles,
  getRects,
} from './utils';

chai.use(chaiEnzyme());
should();


describe('TileGrid Row spec', () => {
  it('should contain values when create', () => {
    const row = new Row({ width: 100 });
    row.should.be.instanceof(Row);
    row.should.have.property('width', 100);
    row.should.have.property('minHeight', 9999);
  });
  it('should recalculate minHeight when Item add', () => {
    const row = new Row();
    row.should.have.property('minHeight', 9999);
    row.add({ height: 100 });
    row.should.have.property('minHeight', 100);
  });
  it('should set height for own items', () => {
    const row = new Row({ width: 900 });
    const items = [
      createItem({ json_img: { width: 400, height: 200 } }),
      createItem({ json_img: { width: 400, height: 300 } }),
    ];
    row.add(items[0]);
    row.add(items[1]);
    row.calculate();
    items[1].should.have.property('height', 200);
  });
  it('should not change width when not filled', () => {
    const row = new Row({ width: 701 });
    const items = [
      createItem({ json_img: { width: 200, height: 100 } }),
      createItem({ json_img: { width: 300, height: 100 } }),
    ];
    row.add(items[0]);
    row.add(items[1]);
    row.calculate();
    row.should.have.property('filled', false);
    items[0].should.have.property('width', 200);
    items[1].should.have.property('width', 300);
  });
  it('should set left', () => {
    const row = new Row({ width: 701 });
    const items = [
      createItem({ json_img: { width: 200, height: 100 } }),
      createItem({ json_img: { width: 300, height: 100 } }),
    ];
    row.add(items[0]);
    row.add(items[1]);
    row.calculate();
    row.should.have.property('filled', false);
    items[0].should.have.property('left', 0);
    items[1].should.have.property('left', 200);
  });
  it('should set top', () => {
    const row = new Row({ width: 701, top: 250 });
    const items = [
      createItem({ json_img: { width: 200, height: 100 } }),
      createItem({ json_img: { width: 300, height: 100 } }),
    ];
    row.add(items[0]);
    row.add(items[1]);
    row.calculate();
    row.should.have.property('filled', false);
    items[0].should.have.property('top', 250);
    items[1].should.have.property('top', 250);
  });
  it('should not change width when filled and width ratio 1', () => {
    const row = new Row({ width: 800 });
    const items = [
      createItem({ json_img: { width: 200, height: 100 } }),
      createItem({ json_img: { width: 300, height: 100 } }),
      createItem({ json_img: { width: 300, height: 100 } }),
    ];
    items.forEach((item) => {
      row.add(item);
    });
    row.calculate();
    row.should.have.property('filled', true);
    items[0].should.have.property('width', 200);
    items[1].should.have.property('width', 300);
    items[2].should.have.property('width', 300);
  });
  it('should change width when filled and width ratio  not 1', () => {
    const row = new Row({ width: 900 });
    const items = [
      createItem({ json_img: { width: 800, height: 100 } }),
      createItem({ json_img: { width: 400, height: 100 } }),
    ];
    items.forEach((item) => {
      row.add(item);
    });
    row.calculate();
    row.should.have.property('filled', true);
    items[0].should.have.property('width', 600);
    items[1].should.have.property('width', 300);
  });
  it('should scale width when filled ', () => {
    const row = new Row({ width: 1200, filled: true });
    const items = [
      createItem({ json_img: { width: 200, height: 390 } }),
      createItem({ json_img: { width: 400, height: 390 } }),
    ];
    items.forEach((item) => {
      row.add(item);
    });
    row.calculate();
    row.should.have.property('filled', true);
    items.reduce((total, { width }) => total + width, 0).should.be.equal(1200);
    items[0].should.have.property('width', 400);
    items[1].should.have.property('width', 800);
  });
  it('should return vertical', () => {
    const row = new Row({ width: 2200 });
    const items = [
      createItem({ json_img: { width: 200, height: 790 } }),
      createItem({ json_img: { width: 600, height: 300 } }),
      createItem({ json_img: { width: 200, height: 790 } }),
      createItem({ json_img: { width: 200, height: 300 } }),
    ];
    items.forEach((item) => {
      row.add(item);
    });
    row.calculate();
    row.should.have.deep.property('verts');
    row.verts.should.have.length(2);
  });
});
describe('TileGrid Item spec', () => {
  it('should contain values when create', () => {
    const item = new Item({ id: 1, img: { width: 2, height: 3 } });
    item.should.be.instanceof(Item);
    item.should.have.property('id', 1);
    item.should.have.deep.property('img.width', 2);
    item.should.have.deep.property('img.height', 3);
    item.should.have.property('width', 2);
    item.should.have.property('height', 3);
    item.should.have.property('top', 0);
    item.should.have.property('left', 0);
  });
  it('should mind height when vertical', () => {
    const item = createItem({ id: 1, json_img: { width: 200, height: 450 } });
    item.should.be.instanceof(VerticalItem);
    item.setHeight(250);
    item.should.have.property('height', 200);
    item.should.have.property('fillHeight', 250);
  });
  it('should be filled when vertical', () => {
    const item = createItem({ id: 1, json_img: { width: 200, height: 450 } });
    item.should.be.instanceof(VerticalItem);
    item.should.have.property('filled', false);
    item.setHeight(300);
    item.should.have.property('filled', true);
  });
  it('should not be filled when vertical width bigger', () => {
    const item = createItem({ id: 1, json_img: { width: 350, height: 450 } });
    item.should.be.instanceof(VerticalItem);
    item.should.have.property('filled', false);
    item.setHeight(300);
    item.should.have.property('filled', false);
    item.should.have.property('height', 9999);
  });
  it('should not set width  less 200', () => {
    let item = createItem({ id: 1, json_img: { width: 350, height: 450 } });
    item.setWidth(190);
    item.should.have.property('width', 200);
    item = createItem({ id: 1, json_img: { width: 500, height: 150 } });
    item.setWidth(190);
    item.should.have.property('width', 200);
  });
});
describe('TileGrid utils spec', () => {
  it('normalize should normalized item data', () => {
    const item = normalize({
      id: 'foo',
      medium_img: 'bar',
      json_img: {
        width: 500,
        height: 200,
      },
      created: 1,
      title: 'foo',
      href: 'bar',
    });

    item.should.have.property('id', 'foo');
    item.should.have.deep.property('img.src', 'bar');
    item.should.have.deep.property('img.width', 500);
    item.should.have.deep.property('img.height', 200);
    item.should.have.property('created', 1);
    item.should.have.property('title', 'foo');
    item.should.have.property('href', 'bar');
  });
  it('createItem should create new normalized item', () => {
    const item = createItem({
      id: 'foo',
      medium_img: 'bar',
      json_img: {
        width: 500,
        height: 200,
      },
      created: 1,
      title: 'foo',
      href: 'bar',
    });

    item.should.be.instanceof(Item);
    item.should.have.property('id', 'foo');
    item.should.have.deep.property('img.src', 'bar');
    item.should.have.deep.property('img.width', 500);
    item.should.have.deep.property('img.height', 200);
    item.should.have.deep.property('width', 500);
    item.should.have.deep.property('height', 200);
    item.should.have.property('created', 1);
    item.should.have.property('title', 'foo');
    item.should.have.property('href', 'bar');
  });
  it('revertSortByCreated should revert sorted items by time', () => {
    revertSortByCreated([
      {
        id: 'foo',
        created: 1.5,
      },
      {
        id: 'bar',
        created: 1.3,
      },
    ]).map(({ id }) => id).should.be.deep.equal(['bar', 'foo']);
  });
  it('getRects calculate rects from verts array', () => {
    const verts = [
      new VerticalItem({ left: 0, width: 100 }),
      new VerticalItem({ left: 500, width: 100 }),
      new VerticalItem({ left: 300, width: 100 }),
      new VerticalItem({ left: 400, width: 100, filled: true }),
      new VerticalItem({ left: 600, width: 100 }),
    ];

    getRects(verts, 900).should.be.deep.equal([[100, 300], [400, 500], [700, 900]]);
  });
});
describe('Issues from log', () => {
  const files = [
    // 'out1.json',
    'out2.json',
    // 'out3.json',
    // 'out4.json',
    //'out5.json',
  ];

  files.forEach((file) => {
    const filename = `./__test/${file}`;
    const json = require(filename);
    it(`${json.title} (${file})`, () => {
      const items = getTiles(json.in, json.width);

      json.out.forEach((item, key) => {
        const element = items[key];
        element.should.have.property('top', item.top);
        element.should.have.property('left', item.left);
        element.should.have.property('width', item.width);
        element.should.have.property('height', item.height);
        if (item.realHeight) {
          element.realHeight().should.be.equal(item.realHeight);
        }
      });
    });
  });
});
