import React, { Component, PropTypes } from 'react';
import TileImageCard from '../TileImageCard/TileImageCard';

import {
  getTiles,
} from './utils.js';

import './style.scss';

export default class TileGrid extends Component {
  static propTypes = {
    items: PropTypes.array.isRequired,
  }

  static defaultProps ={
    items: [],
  }

  constructor(props) {
    super(props);
    this.state = { width: 0 };
    this.container = null;
    this.items = [];
  }

  componentDidMount() {
    setTimeout(this.handleStop(), 100);
    setTimeout(this.handleStop(), 700);

    const last = {
      value: 0,
    };
    let timeout = null;
    const fn = this.handleStop();

    window.addEventListener('resize', () => {
      if (!last.value || Math.abs(this.container.clientWidth - last.value) > 5) {
        last.value = this.container.clientWidth;
        fn();
      }
      clearTimeout(timeout);
      timeout = setTimeout(this.handleStop(), 700);
    });
  }

  setWidth(width) {
    this.setState({ width });
  }

  getGrid() {
    const { width } = this.state;
    if (!width) {
      return <noscript />;
    }
    this.updateTiles(width);
    // global.log.out = flat(items);
    // global.log.width = width;
    return this.items.map(item => (<TileImageCard key={`card-${item.id}`} item={item} />));
  }

  updateTiles(width) {
    this.items = getTiles(this.props.items, width);
  }

  handleStop() {
    return () => {
      if (this.state.width !== this.container.clientWidth) {
        this.setWidth(this.container.clientWidth);
      }
    };
  }

  render() {
    const grid = this.getGrid();
    let realHeight = 0;
    this.items.forEach((item) => {
      realHeight = Math.max(realHeight, item.top + item.realHeight());
    });
    const style = {
      height: realHeight,
    };
    return (
      <div
        style={style}
        className="tile-grid"
        ref={(ref) => { this.container = ref; }}
      >
        { grid }
      </div>
    );
  }
}
