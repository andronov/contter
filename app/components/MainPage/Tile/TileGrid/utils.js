/* eslint camelcase:0 */
import classnames from 'classnames';

const MAGIC = 275;
const BIG_MAGIC = 9999;
const SMALL_MAGIC = 10;

const MAX_HEIGHT = 450;
const RANDOM = 100;

export function normalize({
    id,
    medium_img,
    item,
  json_img: {
    width,
    height,
  },
    created,
    title,
    href,
}) {
  let nextHeight = height;
  let nextWidth = width;

  if (height > width) {
    nextHeight = Math.floor(Math.random() * (750- 450) + 200);
    nextWidth = Math.floor(Math.random() * (nextHeight -10 - 400) + 450);
  } else {
    nextWidth = Math.floor(Math.random() * (750 - 400) + 400);
    nextHeight = Math.floor(Math.random() * (nextWidth - 10 - 250) + 250);
  }

  return {
    id,
    item,
    img: {
      src: medium_img,
      height,
      width,
    },
    created,
    title,
    href,
    box: {
      height: nextHeight,
      width: nextWidth,
    },
  };
}
export class Item {

  constructor(opts) {
    let size = {};
    if (opts.img) {
      size = { width: opts.box.width, height: opts.box.height };
    }

    Object.assign(
      this,
      {
        top: 0,
        left: 0,
        className: '',
      },
      size,
      opts);
  }

  setHeight(height) {
    this.height = height;
  }

  setWidth(width) {
    this.width = Math.max(width, MAGIC);
  }

  setLeft(left) {
    this.left = left;
  }

  setTop(top) {
    this.top = top;
  }

  realHeight() {
    return this.height;
  }

  addClassName(className) {
    this.className = classnames(this.className, className);
  }
}


export class VerticalItem extends Item {
  constructor(opts) {
    super({
      filled: false,
      fillHeight: 0,
      ...opts,
    });
  }

  setHeight(height) {
    this.height -= height;
    this.fillHeight += height;
    if ((this.width < this.fillHeight) &&
        ((this.fillHeight >= 500) ||
         (this.fillHeight >= this.box.height - 250))) {
      this.filled = true;
    }
    if (this.height < 0) {
      this.height = 0;
    }
  }

  realHeight() {
    return this.filled ? this.fillHeight : this.box.height;
  }
}

export class HorizontalItem extends Item {
}

export class Row {
  constructor(opts) {
    Object.assign(
      this,
      {
        minHeight: BIG_MAGIC,
        minWidth: BIG_MAGIC,
        filled: false,
        items: [],
        cleared: [],
        actualWidth: 0,
        verts: [],
        left: 0,
        top: 0,
      },
      opts,
    );
  }

  add(item) {
    this.items.push(item);
    this.minHeight = Math.min(this.minHeight, item.height);
    if (item instanceof HorizontalItem) {
      this.minWidth = Math.min(this.minWidth, item.width);
    }

    this.actualWidth += item.width;
    if (this.actualWidth >= this.width - 300) {
      this.filled = true;
      const lostWidth = this.width;
      const nextWidth = this.actualWidth;
      const ratio = this.filled ? lostWidth / nextWidth : 1;
      this.minHeight = Math.min(this.minHeight, (this.minWidth * ratio) - SMALL_MAGIC);
    }
  }

  calculate() {
    let lostWidth = this.width;
    let nextWidth = this.actualWidth;
    let ratio = this.filled ? lostWidth / nextWidth : 1;
    let left = this.left;
    this.cleared = [];
    this.minHeight = Math.max(250, this.minHeight);
    this.items.forEach((item) => {
      ratio = this.filled ? lostWidth / nextWidth : 1;
      item.setHeight(this.minHeight);
      nextWidth -= item.width;
      item.setWidth(Math.floor(item.box.width * ratio));
      lostWidth -= item.width;
      if (lostWidth < MAGIC) {
        item.setWidth(item.width + lostWidth);
      }
      item.setLeft(left);
      item.setTop(this.top);
      if (this.top === 0) {
        item.addClassName('no-top');
      }
      if (left === 0) {
        item.addClassName('no-left');
      }
      left += item.width;
      if (left >= this.globalWidth) {
        item.addClassName('no-right');
      }
      if (item instanceof VerticalItem && !item.filled) {
        this.verts.push(item);
      }
    });
  }

  setMinHeight(minHeight) {
    this.minHeight = minHeight;
  }
}

export function getRects(verts, width) {
  const ret = [];
  const sorted = verts.filter(({ filled }) => !filled).sort((cur, next) => cur.left - next.left);

  let left = 0;
  sorted.forEach((item) => {
    if (!item.filled) {
      const right = item.left + item.width;
      if (left !== item.left) {
        ret.push([left, item.left]);
      }
      left = right;
    }
  });

  if (left !== 0 && left !== width) {
    ret.push([left, width]);
  }


  return ret;
}

export class CompositeRow {
  constructor(opts) {
    Object.assign(
      this,
      {
        rows: [],
        cur: 0,
        items: [],
      },
      opts,
    );
    this.rows = getRects(this.verts, this.width).map(([left, right]) => new Row({
      ...opts,
      left,
      width: right - left,
    }));
  }

  add(item) {
    if (this.rows[this.cur].filled) {
      this.cur++;
    }
    this.rows[this.cur].add(item);
  }

  get filled() {
    return this.rows.every(({ filled }) => filled);
  }

  get minHeight() {
    return this.rows.reduce((minHeight, row) => Math.min(minHeight, row.minHeight), BIG_MAGIC);
  }

  calculate() {
    this.verts.forEach((item) => {
      item.setHeight(this.minHeight);
    });
    this.rows.forEach((row) => {
      row.setMinHeight(this.minHeight);
      row.calculate();
      this.items = this.items.concat(row.items);
    });
  }
}

export function createItem(data) {
  if (data.box.height > data.box.width) {
    return new VerticalItem(data);
  }

  return new HorizontalItem(data);
}

export function revertSortByCreated(items) {
  return items.sort((cur, next) => cur.created - next.created);
}

export function getTiles(items, width) {
  const nextItems = [...items];
  console.log(items);
  let ret = [];
  let top = 0;
  let verts = [];
  while (nextItems.length) {
    verts = verts.filter(({ filled }) => !filled);
    let row = null;
    if (!verts.length) {
      row = new Row({ width, top, verts, globalWidth: width });
    } else {
      row = new CompositeRow({ width, top, verts, globalWidth: width });
    }

    while (nextItems.length && !row.filled) {
      row.add(createItem(nextItems.pop()));
    }
    row.calculate();
    ret = ret.concat(row.items);
    top += row.minHeight;
  }
  console.log('====');
  ret.forEach((item) => console.log(item.realHeight(), item.width));
  return ret;
}

export function normalizeItems(items) {
  return revertSortByCreated(items).map(item => normalize(item));
}
