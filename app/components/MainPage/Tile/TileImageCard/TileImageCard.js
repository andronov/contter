import React, { PropTypes } from 'react';
import ImageCard from '../../ImageCard/ImageCard';


import {
  VerticalItem,
} from '../TileGrid/utils.js';

export default function TileImageCard({ item }) {
  const prop = {
    ...item,
  };
  if (item instanceof VerticalItem) {
    prop.height = item.realHeight();
  }

  return (<ImageCard {...prop} />);
}

TileImageCard.propTypes = {
  item: PropTypes.object.isRequired,
};
