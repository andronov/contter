import React from 'react';
import TileGridDirector from './TileGrid/TileGrid';
import { normalizeItems } from './TileGrid/utils';



export default function TileGrid({ items }) {
  return (<TileGridDirector items={normalizeItems(items)} />);
}

export TileImageCard from './TileImageCard/TileImageCard';