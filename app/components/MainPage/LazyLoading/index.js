import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';

@inject("appState")
@observer
export default class LazyLoading extends React.Component {


  render() {
    return (
      <div className={"lazy " + this.props.className}
      >{this.props.children}</div>
    );
  }
}