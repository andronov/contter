import React from 'react';
import { storiesOf } from '@kadira/storybook';

import { ImageCard } from 'components';

storiesOf('ImageCard', module)
  .add('with 640x480', () => {
    const property = {
      title: 'Test message for upyr-people',
      imgSrc: 'https://dummyimage.com/640x480/000/fff',
      width: 340,
      height: 220,
      size: {
        width: 640,
        height: 480,
      },
      classNames: '',
    };
    return (
      <ImageCard {...property} />
    );
  })
  .add('with 480x640', () => {
    const property = {
      title: 'Test message for upyr-people',
      imgSrc: 'https://dummyimage.com/480x640/000/fff',
      width: 340,
      height: 220,
      size: {
        width: 480,
        height: 640,
      },
      classNames: '',
    };
    return (
      <ImageCard {...property} />
    );
  });
