/* eslint-disable */
import React from 'react';
import chai, { should } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import { shallow } from 'enzyme';

import
  ImageCard,
  { offsetForCenterPosition }
from './ImageCard';

chai.use(chaiEnzyme());
should();

describe('@Ignored ImageCard spec', () => {
  /*
  const baseProp = {
    title: 'Title',
    imgSrc: 'https://dummyimage.com/640x480/000/fff',
    width: 340,
    height: 220,
  };
  it('should render component', () => {
    const imageSize = {
      size: {
        width: 640,
        height: 480,
      },
    };
    const prop = Object.assign(baseProp, imageSize);
    const element = shallow(<ImageCard {...prop} />);
    element.find('div').should.be.present();
    element.find('a').should.be.present();
    element.find('img').should.be.present();
    element.find('span').should.be.present();
    element.find('.image-card').should.have.style('width', '340px');
    element.find('.image-card').should.have.style('height', '220px');
    element.find('.image-card').should.have.style('top', '0');
    element.find('.image-card').should.have.style('left', '0');
  });
  it('should render img 640x180 to 340x220 container. Height stretch', () => {
    const imageSize = {
      size: {
        width: 640,
        height: 180,
      },
    };
    const prop = Object.assign(baseProp, imageSize);
    const element = shallow(<ImageCard {...prop} />);
    element.find('.image').should.have.style('top', '0');
  });
  it('should render img 640x480 to 340x220 container. Only by center', () => {
    const imageSize = {
      size: {
        width: 640,
        height: 480,
      },
    };
    const prop = Object.assign(baseProp, imageSize);
    const element = shallow(<ImageCard {...prop} />);
    const { height, width, size } = prop;
    element.find('.image').should.have.style('left', `${offsetForCenterPosition(width, size.width)}px`);
    element.find('.image').should.have.style('top', `${offsetForCenterPosition(height, size.height)}px`);
  });
  it('should render img 480x640 to 340x220 container. Only by center', () => {
    const imageSize = {
      size: {
        width: 480,
        height: 640,
      },
    };
    const prop = Object.assign(baseProp, imageSize);
    const element = shallow(<ImageCard {...prop} />);
    const { height, width, size } = prop;
    element.find('.image').should.have.style('left', `${offsetForCenterPosition(width, size.width)}px`);
    element.find('.image').should.have.style('top', `${offsetForCenterPosition(height, size.height)}px`);
  });
  it('should render img 280x640 to 340x220 container. Width stretch', () => {
    const imageSize = {
      size: {
        width: 280,
        height: 640,
      },
    };
    const prop = Object.assign(baseProp, imageSize);
    const element = shallow(<ImageCard {...prop} />);
    element.find('.image').should.have.style('left', '0');
  });
   */
});
