import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import deepEqual from 'deep-equal';
import Item from '../Item';

import './style.scss';

export function crop(imgWidth, imgHeight, secWidth, secHeight) {
  let K = 0;
  if (imgHeight > imgWidth && imgHeight < secHeight) {
    K = secHeight / imgHeight;
  } else {
    K = secWidth / imgWidth;
  }
  return K;
}

export function offsetForCenterPosition(objA, objB) {
  return Math.round((objA - objB) / 2);
}

export default class ImageCard extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    img: PropTypes.object.isRequired,
    href: PropTypes.string,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    top: PropTypes.number.isRequired,
    left: PropTypes.number.isRequired,
    className: PropTypes.string,
    item: PropTypes.object
  };

  static defaultProps = {
    href: '/',
    className: '',
  };
  shouldComponentUpdate(nextProps) {
    return !deepEqual(this.props, nextProps);
  }

  render() {
    console.log('ImageCard', this);
    const {
      title, item, img: { src, ...size }, href,
      width, height, top, left, className,
    } = this.props;
    const nextClassName = classnames(className, 'image-card');
    const styleContainer = {
      width,
      height,
      top,
      left,
    };

    let K = 1;
    if (size.width < width) {
      K = width / size.width;
    }
    if (size.height < height) {
      K = Math.max(K, height / size.height);
    }

    const thumbWidth = Math.floor(K * size.width);
    const thumbHeight = Math.floor(K * size.height);
    const styleImg = {
      width: thumbWidth,
      height: thumbHeight,
      top: offsetForCenterPosition(height, thumbHeight),
      left: offsetForCenterPosition(width, thumbWidth),
    };


    return (
      <div className={nextClassName} style={styleContainer}>
        <Item grid={true} widthBlock={width} heightBlock={height} styleImg={styleImg} item={item} />
      </div>
    );
  }
}
