import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import ColorPicker from './ColorPicker';
import { rgba } from '../../../services/utils';
import * as uuid from 'uuid';
import Rating from '../Item/Rating';
import Link from '../../Link';
import Heart from '../Item/Heart';
import Tabs from '../Header/HeaderMenu/Tabs';

let points = [];
let painters = [];
let mouseX = 0;
let mouseY = 0;
let stickersG = [{
  id: 1,
  uuid: null,
  path: 'http://www.stickpng.com/assets/thumbs/580b57fbd9996e24bc43be2a.png',
  cat: 1,
  popularity: 100
},
  {
    id: 2,
    uuid: null,
    path: 'http://www.stickpng.com/assets/thumbs/588a3cc6d06f6719692a2d0a.png',
    cat: 1,
    popularity: 100
  },
  {
    id: 3,
    uuid: null,
    path: 'https://i.giphy.com/l3q2AuZRzjmzRie1G.gif',
    cat: 1,
    popularity: 101
  }
];

let catStickers = [
  {
    id: 1,
    name: 'Recent',
    onlyIcon: true,
    className: 'cat_st_recent'
  },
  {
    id: 2,
    name: 'Celebrities',
    haveGif: true
  },
  {
    id: 3,
    name: 'gif',
    isGif: true,
    className: 'cat_st_gif'
  },
  {
    id: 4,
    name: 'At the Movies'
  },
  {
    id: 5,
    name: 'Comics and Fantasy'
  },
  {
    id: 6,
    name: 'Food'
  },
  {
    id: 7,
    name: 'Holidays'
  }
];

function midPointBtw(p1, p2) {
  return {
    x: p1.x + (p2.x - p1.x) / 2,
    y: p1.y + (p2.y - p1.y) / 2
  };
}

@inject("appState")
@observer
export default class ShareItem extends React.Component {

  state = {
    canvas: null,
    context: null,
    drawing: false,
    currentCanvas: null,
    drawingText: false,
    editorMode: false,
    lastX: 0,
    lastY: 0,
    lineWidth: 5,
    stickerMode: false,
    activeSticker: false,
    activeStickerItem: null,
    activeStickerDiv: null,
    activeStickerSize: false,
    activeStickerRotate: false,
    activeStickerDrop: false,
    activeStickers: [],
    brushType: null,
    toggleText: false,
    activeTextDivTyping: false,
    activeTextDivActive: false,
    activeTextDivRotate: false,
    activeTextDrawing: false,
    activeTextDiv: null,
    inputText: '',
    textActive: false,
    drawMode: false,
    canvasTexts: [],
    stickers: stickersG,
    catStickers: catStickers,
    activeCatStickers: catStickers[0],
    selectedText: -1,
    mentionText: '',
    hashtagText: '',
    brushColor: 'rgb(0,175,255)',
    mouseX: 0,
    mouseY: 0,
    canvasLayerTexts: [],
    history: [],
    undoList: [],
    redoList: []
  };


  componentDidMount(){
    let canvas = document.querySelector('.canvas');
    let { item} = this.props;
    let {width, height} = item.arcImage(canvas.parentNode.width, 1300);

    canvas.width  = width;
    canvas.height = height;

    canvas.parentNode.style = `width: ${width}px; height: ${height}px;`;

    let ctx = canvas.getContext('2d');

    ctx.lineWidth = 10;
    ctx.lineJoin = ctx.lineCap = 'round';

    this.setState({
      canvas: canvas,
      context: ctx
    });


    let image = new window.Image();
    image.width = width;
    image.crossOrigin = "";
    image.height = height;
    image.src = item.img;
    image.onload = function() {
      ctx.drawImage(image, 0, 0, width, height)
    };

    window.addEventListener('mousemove', this.handleOnMouseMoveBlock);
    window.addEventListener('touchmove', this.handleOnMouseMoveBlock);

    window.addEventListener('mousedown',  this.handleOnMouseDownBlock);
    window.addEventListener('touchdown',  this.handleOnMouseDownBlock);

    window.addEventListener('mouseup',  this.handleOnMouseUpBlock);
    window.addEventListener('touchend',  this.handleOnMouseUpBlock);
    this.activeXPos = 0;
    this.activeYPos = 0;
    //console.log('requestAnimFrame', window.requestAnimFrame);
    //this.animateDrawGif();
  };


  componentWillUnmount() {
    window.removeEventListener('mousemove', this.handleOnMouseMoveBlock);
    window.removeEventListener('touchmove', this.handleOnMouseMoveBlock);

    window.removeEventListener('mousedown',  this.handleOnMouseDownBlock);
    window.removeEventListener('touchdown',  this.handleOnMouseDownBlock);

    window.removeEventListener('mouseup',  this.handleOnMouseUpBlock);
    window.removeEventListener('touchend',  this.handleOnMouseUpBlock);
  }

  handleOnMouseDownBlock = (e) => {
    let { activeTextDivTyping, activeStickerDrop } = this.state;
    if(activeTextDivTyping && !e.target.classList.contains('cr_text__textarea')){
      this.setState({activeTextDivTyping : false, activeTextDivActive: false});
    }
    if(e.target.classList.contains('cr_text__selection_handler')){
      this.setState({activeStickerSize: true, activeStickerRotate: false});
    }
    if(e.target.classList.contains('cr_text__selection_rotate')){
      this.setState({activeStickerRotate: true, activeStickerSize: false});
    }
  };

  handleOnMouseUpBlock = (e) => {
    this.setState({activeStickerDrop: false,
      activeStickerSize: false,
      activeStickerRotate: false});
  };

  handleOnMouseMoveBlock = (e) => {
    let { activeTextDiv, activeTextDivTyping,
      activeTextDrawing, activeStickerItem,
      activeStickerDiv,
      activeStickerDrop,
      activeStickerSize,
      activeStickerRotate,
      activeTextDivRotate} = this.state;
    let x = 0,
      y = 0;
    this.activeXPos = e.clientX;
    this.activeYPos = e.clientY;
    if(activeStickerItem
      && activeStickerDiv && activeStickerDrop){
      x = this.activeXPos - this.activeStickerDivX;
      y = this.activeYPos - this.activeStickerDivY;


      if(activeStickerSize){
        let w = Math.max(this.activeXPos - activeStickerDiv.getBoundingClientRect().left, 30);
        activeStickerDiv.style.width = w + 'px';
        activeStickerDiv.style.height = w + 'px';
        activeStickerDiv.firstChild.firstChild.style.width = w + 'px';
        activeStickerDiv.firstChild.firstChild.style.height = w + 'px';

        return
      }
      if(activeStickerRotate){
        let center_x = (activeStickerDiv.getBoundingClientRect().left) + (activeStickerDiv.offsetHeight/2);
        let center_y = (activeStickerDiv.getBoundingClientRect().top) + (activeStickerDiv.offsetWidth/2);
        let radians = Math.atan2(x-center_x, y-center_y);
        let degree = (radians * (180 / Math.PI) * -1) + 90;
        // console.log('activeStickerRotate', radians, degree);
        activeStickerDiv.style.transform = "rotateZ("+degree+"deg)";
        return
      }

      activeStickerDiv.style.top = `${y}px`;
      activeStickerDiv.style.left = `${x}px`;
    }
  };

  handleOnMouseDownSticker = (e, item) => {
    let cTarget = e.currentTarget;

    this.activeStickerDivX = this.activeXPos - cTarget.offsetLeft;
    this.activeStickerDivY = this.activeYPos - cTarget.offsetTop;

    this.setState({activeStickerItem: item,
      activeStickerDrop: true,
      activeStickerDiv: cTarget});
  };

  handleOnMouseUpSticker = (e, item) => {
    this.setState({activeStickerDrop: false,
      activeStickerSize: false,
      activeStickerRotate: false});
  };


  renderItemContent = (item) => {
    return(
      <div className='fit-content'>
        <div className='fit-item-image-container card__image'>

          <div className="card__overlay card__overlay--indigo">
            <div className="card__content">
              <div className="card__content__block">
                {/*<div className="card__content__rating">
                 <Rating item={item} />
                 </div>*/}
                <div className="card__content__title">
                  <div className="card__content__title__link">
                    <a target='_black' href={item.url} className="card__title">{item.title}</a>
                  </div>
                  <div className="card__content__title__source">

                    <div className="card__content__title__source_top">
                      <div>
                        {item.wallType !== 5 ? <Link type="profile" style={{textDecoration: 'none'}}
                                                     href={`/${item.site.slug}`} value={item.site.short_url} to={`/${item.site.slug}`} {...this.props}/> : null}
                        {item.wallType === 5 ?
                          <div>
                            <a title={item.user.first_name} href={'/'+item.user.username}>@{item.user.username}</a>
                            {item.site.short_url ? ' from ' : ''}
                            {item.site.short_url ?
                              <Link type="profile" style={{textDecoration: 'none'}}
                                    href={`/${item.site.slug}`} value={item.site.short_url} to={`/${item.site.slug}`} {...this.props}/>
                              : null}
                          </div>
                          : null}
                      </div>
                      <div className="card__content__time">
                        {item.fromNow() }
                      </div>
                      <div className="card__content__rating">
                        <Rating Rating={this.Rating} item={item} />
                      </div>
                    </div>

                    <div className="card__content__title__source_bottom">
                      <div className="card__content__button">
                        <div onClick={(it) => this.Share(item)} className="card__content__share">
                          Share +15
                        </div>

                        <Heart Like={this.Like} item={item} {...this.props}/>

                        <div className="card__content__more">
                          <div className="card__content__more_inner">
                            <div />
                            <div />
                            <div />
                          </div>
                        </div>

                      </div>
                    </div>

                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div> )
  };


  setStickerMode = () => {
    this.setState({
      brushType: null,
      textActive: false,
      drawMode: false,
      stickerMode: !this.state.stickerMode,
      editorMode: !this.state.editorMode,
      activeTextDivRotate: false,
      activeTextDivActive: false,
      activeTextDiv: false,
      activeText: false
    })
  };

  onClickSticker = (item) => {
    let { activeStickers } = this.state;
    let activeStickersT = activeStickers;
    item.uuid = uuid.v4();
    activeStickersT.push(item);
    this.setState({activeStickers, activeStickersT,
      activeStickerItem: null,
      activeSticker: true
    });
  };

  render() {

    let { item, user, styleImg, widthBlock, grid , heightBlock} = this.props;
    let { toggleText, drawing, activeTextDivTyping,
      stickerMode, stickers, activeStickers,
      editorMode, catStickers, activeCatStickers,
      activeStickerItem, activeSticker,
      drawMode, activeText, activeTextDiv, activeTextDivActive,
      inputText, canvasLayerTexts } = this.state;
   // console.log('Item2', item.img);
    let {width, height} = item.arcImage(600, 300);
    //console.log('Itme7', width, height);
    let styleImgBase = styleImg;
    let styleItem = styleImg ? styleImg : {width: `${width}px`, height: `${height}px`};
    //if(!grid){styleItem['backgroundImage'] = `url(${item.img})`;}
    if(heightBlock){styleImg.height = heightBlock}
    if(widthBlock){styleImg.width = widthBlock}
    if(grid){delete styleItem['top'];delete styleItem['left'];}




    return (
      <div className="share__block">
        <div className="share__block__top">
          <div className="share_top">
            <div>left</div>
            <div className="share_top__menu">
              <div className="share_top__menu_item share_top__brush b_type_3" />
              <div onClick={(tp) => this.setStickerMode()} className="share_top__menu_item share_top__sticker" />
            </div>
          </div>
        </div>

        <div className="share__block__center">
          <div>1</div>
          <div style={styleImg} className="share_item">
            <div className="canvas_block">
              <div className="item__card card" >
                <canvas style={{pointerEvents: drawMode ? 'all': 'none'}}
                        className="canvas"
                        id="canvas"
                        width={width}
                        height={height}>
                </canvas>
                {!editorMode ? this.renderItemContent(item) : null}
              </div>

              <div style={{pointerEvents: !drawMode ? 'all': 'none'}}
                   className="layer__sticker">
                {activeStickers.map((item, i) => {
                  return(
                    <div
                      key={item.uuid}
                      //onClick={(it) => this.onClickStickerView(item)}
                      onMouseDown = {(e, it) => this.handleOnMouseDownSticker(e, item)}
                      onTouchStart = {(e, it) => this.handleOnMouseDownSticker(e, item)}
                      //onMouseUp = {(e, it) => this.handleOnMouseUpSticker(e, item)}
                      //onTouchEnd = {(e, it) => this.handleOnMouseUpSticker(e, item)}
                      className={`cr_sticker ${(activeStickerItem === item ? ' active ' : null)}`}
                    >
                      <div className="cr_sticker__inner">
                        <img className="cr_sticker__img" src={item.path}/>
                      </div>
                      {activeStickerItem === item ?
                        <div  className="cr_text__selection">
                          {/*<div className="cr_text__selection_handler" id="pe_nw" />*/}
                          {/*<div className="cr_text__selection_handler" id="pe_ne" />*/}

                          <div className="cr_text__selection_handler" id="pe_se" />
                          <div className="cr_text__selection_rotate" id="pe_rt" />
                          {/*<div className="cr_text__selection_handler" id="pe_sw" />*/}
                        </div>
                        : null}
                    </div>
                  )
                })}
              </div>

            </div>
          </div>
          <div>2</div>
        </div>

        <div className="share__block__bottom">

          {stickerMode ?
            <div className="sticker_block">
              <div className="sticker_block__menu">
                <Tabs classnam="sticker_block__menu_item" onTabChange={this.onClickTab} selected={0} tabs={catStickers.map(item=>{return {displayName: item.name, key: item.id}})} />
                {/*catStickers.map(item=>{
                  return(
                    <div className="sticker_block__menu_item">
                      <div className="sticker_block__menu_name">
                        {item.name}
                      </div>
                    </div>
                  )
                })*/}
              </div>
              <div  className="sticker_block__stickers">
                {stickers.map(item=>{
                  return(
                    <div onClick={(it) => this.onClickSticker(item)} key={item.id} className="sticker_block__sticker">
                      <img className="sticker_block__sticker_img" src={item.path}/>
                    </div>
                  )
                })}
              </div>
            </div>
            : null}

        </div>

      </div>
    );
  }
}