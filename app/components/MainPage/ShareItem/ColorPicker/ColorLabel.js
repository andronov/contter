import * as React from 'react';

var ColorLabel = React.createClass({
  propTypes: {
    handleClick: React.PropTypes.func.isRequired,
    color: React.PropTypes.string.isRequired,
    id: React.PropTypes.string,
    isActive: React.PropTypes.bool
  },
  handleClick: function () {
    this.props.handleClick();
  },
  render: function () {
    var styles = {
      //backgroundColor: this.props.color
    };
    return (
      <div>
        <div type='button'
                id={this.props.id}
                className={this.props.isActive ? 'active si_color_btn' : 'si_color_btn'}
                style={styles}
                onClick={this.props.handleClick.bind(this, this.props.id)} />
      </div>
    );
  }
});

export default ColorLabel