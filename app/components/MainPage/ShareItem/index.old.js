import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import ColorPicker from './ColorPicker';
import { rgba } from '../../../services/utils';
import * as uuid from 'uuid';

let points = [];
let painters = [];
let mouseX = 0;
let mouseY = 0;
let stickersG = [{
  id: 1,
  uuid: null,
  path: 'http://www.stickpng.com/assets/thumbs/580b57fbd9996e24bc43be2a.png',
  cat: 1,
  popularity: 100
},
  {
    id: 2,
    uuid: null,
    path: 'http://www.stickpng.com/assets/thumbs/588a3cc6d06f6719692a2d0a.png',
    cat: 1,
    popularity: 100
  },
  {
    id: 3,
    uuid: null,
    path: 'https://i.giphy.com/l3q2AuZRzjmzRie1G.gif',
    cat: 1,
    popularity: 101
  }
];

function midPointBtw(p1, p2) {
  return {
    x: p1.x + (p2.x - p1.x) / 2,
    y: p1.y + (p2.y - p1.y) / 2
  };
}

@inject("appState")
@observer
export default class ShareItem extends React.Component {

  state = {
    canvas: null,
    context: null,
    drawing: false,
    currentCanvas: null,
    drawingText: false,
    lastX: 0,
    lastY: 0,
    lineWidth: 5,
    stickerMode: false,
    activeSticker: false,
    activeStickerItem: null,
    activeStickerDiv: null,
    activeStickerSize: false,
    activeStickerRotate: false,
    activeStickerDrop: false,
    activeStickers: [],
    brushType: null,
    toggleText: false,
    activeTextDivTyping: false,
    activeTextDivActive: false,
    activeTextDivRotate: false,
    activeTextDrawing: false,
    activeTextDiv: null,
    inputText: '',
    textActive: false,
    drawMode: false,
    canvasTexts: [],
    stickers: stickersG,
    selectedText: -1,
    mentionText: '',
    hashtagText: '',
    brushColor: 'rgb(0,175,255)',
    mouseX: 0,
    mouseY: 0,
    canvasLayerTexts: [],
    history: [],
    undoList: [],
    redoList: []
  };

  Share =(item) => {
    let { appState } = this.props;
    console.log('Share', item);
    appState.update({shareItem: item, shareOpen: true});
    appState.router.navigate('/share');

  };

  componentDidMount(){
    let canvas = document.querySelector('.canvas');
    console.log('canvas', canvas);
    console.dir(canvas);
    let { item} = this.props;
    let {width, height} = item.arcImage(canvas.parentNode.width, 1300);

    /* canvas.style.width = '100%';
     canvas.style.height = '100%';*/
    canvas.width  = width;
    canvas.height = height;

    canvas.parentNode.style = `width: ${width}px; height: ${height}px;`;

    let ctx = canvas.getContext('2d');

    ctx.lineWidth = 10;
    ctx.lineJoin = ctx.lineCap = 'round';

    this.setState({
      canvas: canvas,
      context: ctx
    });

    console.log('ctx', ctx);


    let image = new window.Image();
    image.width = width;
    image.crossOrigin = "";
    image.height = height;
    image.src = item.img;
    image.onload = function() {
      ctx.drawImage(image, 0, 0, width, height)
    };


    painters = [];
    for(var i = 0; i < 30; i++) {
      var ease = Math.random() * 0.05 + .7;
      painters.push({
        dx : width / 2,
        dy : width / 2,
        ax : 0,
        ay : 0,
        div : 0.1,
        ease : ease
      });
    }

    window.addEventListener('mousemove', this.handleOnMouseMoveBlock);
    window.addEventListener('touchmove', this.handleOnMouseMoveBlock);

    window.addEventListener('mousedown',  this.handleOnMouseDownBlock);
    window.addEventListener('touchdown',  this.handleOnMouseDownBlock);
    this.activeXPos = 0;
    this.activeYPos = 0;
    //console.log('requestAnimFrame', window.requestAnimFrame);
    //this.animateDrawGif();
  };

  animateDrawGif = () => {
    window.requestAnimFrame(this.animateDrawGif);
    this.drawGif()
  };

  drawGif = () => {
    //file:///C:/Users/user/PycharmProjects/contest2/gif/435c7f29-825_image-0.png
    let canvas = document.querySelector('.canvas');
    let ctx = canvas.getContext('2d');
    let i = 0;
    while(i<=9){
      let width = 300;
      let height = 300;
      let image = new window.Image();
      image.width = width;
      image.crossOrigin = "";
      image.height = height;
      image.src = `/435c7f29-825_image-${i}.png`;
      //image.onload = function() {
      ctx.drawImage(image, 0, 0, width, height);
      //};
      i++
    }
  };

  componentWillUnmount() {
    window.removeEventListener('mousemove', this.handleOnMouseMoveBlock);
    window.removeEventListener('touchmove', this.handleOnMouseMoveBlock);

    window.removeEventListener('mousedown',  this.handleOnMouseDownBlock);
    window.removeEventListener('touchdown',  this.handleOnMouseDownBlock);
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.clear){
      this.resetCanvas();
    }
  };

  textHittest = (x, y, textIndex) => {
    let text = this.state.canvasTexts[textIndex];
    return (x >= text.x && x <= text.x + text.width && y >= text.y - text.height && y <= text.y);
  };

  handleOnMouseDownText = (e, item) => {
    let target = e.target;
    console.log('handleOnMouseDownTe22xt', target);

    if(target.classList.contains('cr_text__selection_handler')){
      this.setState({activeTextDivRotate: true,
        activeTextDrawing: true,
        activeTextDiv: target.parentNode.parentNode});
      return
    }

    let { activeTextDiv, activeTextDivActive } = this.state;
    let activeTextDivTyping = false;
    if(activeTextDiv === e.currentTarget && activeTextDivActive){
      activeTextDivTyping = true;
    }
    this.setState({activeTextDiv: e.currentTarget,
      activeTextDivTyping,
      activeTextDivRotate: false,
      activeTextDivActive: true,
      activeTextDrawing: true});
    console.log('handleOnMouseDownText', item);
  };

  handleOnMouseDown = (e) => {
    let rect = this.state.canvas.getBoundingClientRect();

    //this.state.context.beginPath();
    if(this.isMobile()){
      this.setState({
        lastX: e.targetTouches[0].pageX - rect.left,
        lastY: e.targetTouches[0].pageY - rect.top
      });
    }
    else{
      this.setState({
        lastX: e.clientX - rect.left,
        lastY: e.clientY - rect.top,
        mouseX: e.clientX - rect.left,
        mouseY: e.clientY - rect.top
      });
    }

    /* if(this.state.textActive){
     let check = false;
     this.state.canvasTexts.forEach((text, i)=>{
     if (this.textHittest(this.state.lastX, this.state.lastY, i)) {
     this.setState({selectedText: i, drawingText: true});
     check = true;
     }
     });
     if(!check){this.setState({selectedText: -1, drawingText: false})}
     return;
     }*/

    if(!this.state.brushType)return;
    //ссхраняем предыдущее состояние
    this.saveToUndoHistory();


    if(this.state.brushType === 2){

      for(var i = 0; i < painters.length; i++) {
        painters[i].dx = e.clientX - rect.left;
        painters[i].dy = e.clientY - rect.top;
      }

      let that = this;
      this.IntervalBrushTwo = setInterval(function () {
        let i;
        //  let mouseX = that.state.mouseX;
        //   let mouseY = that.state.mouseY;
        if(that.state.drawing) {
          let ctx = that.state.context;
          ctx.lineWidth = that.state.lineWidth;
          ctx.strokeStyle = that.state.brushColor;
          for (i = 0; i < painters.length; i++) {
            ctx.beginPath();
            var dx = painters[i].dx;
            var dy = painters[i].dy;
            ctx.moveTo(dx, dy);
            var dx1 = painters[i].ax = (painters[i].ax + (painters[i].dx - mouseX) * painters[i].div) * painters[i].ease;
            painters[i].dx -= dx1;
            var dx2 = painters[i].dx;
            var dy1 = painters[i].ay = (painters[i].ay + (painters[i].dy - mouseY) * painters[i].div) * painters[i].ease;
            painters[i].dy -= dy1;
            var dy2 = painters[i].dy;
            ctx.lineTo(dx2, dy2);
            ctx.stroke();
          }
        }
      }, 30);

    }

    this.setState({
      drawing: true,
      drawingText: true
    });
  };

  handleOnMouseMoveBlock = (e) => {
    let { activeTextDiv, activeTextDivTyping,
      activeTextDrawing, activeStickerItem,
      activeStickerDiv,
      activeStickerDrop,
      activeStickerSize,
      activeStickerRotate,
      activeTextDivRotate} = this.state;
    let x = 0,
      y = 0;
    this.activeXPos = e.clientX;
    this.activeYPos = e.clientY;
    /*let el = e.currentTarget;
     let rect = el.getBoundingClientRect();
     let x = parseInt(e.clientX - rect.left);
     let y = parseInt(e.clientY - rect.top);
     if(activeTextDiv && activeTextDrawing && activeTextDivRotate && !activeTextDivTyping) {
     rect = activeTextDiv.getBoundingClientRect();
     x = parseInt(e.clientX - rect.left);
     y = parseInt(e.clientY - rect.top);
     let radians = Math.atan2(x, y);
     let degree = (radians * (180 / Math.PI) * -1) + 180;
     //console.log('rotat7e', "rotate(" + degree + "d)");
     activeTextDiv.style.transform = "rotateZ("+degree+"deg)";
     //activeTextDiv.style = `top: ${y}px; left: ${x}px; transform: rotateZ(${degree}deg);`;
     }

     if(activeTextDiv && activeTextDrawing && !activeTextDivRotate && !activeTextDivTyping){
     activeTextDiv.style.top = `${y}px`;
     activeTextDiv.style.left = `${x}px`;
     // activeTextDiv.style = `top: ${y}px; left: ${x}px;`;
     }*/
    if(activeStickerItem
      && activeStickerDiv && activeStickerDrop){
      x = this.activeXPos - this.activeStickerDivX;
      y = this.activeYPos - this.activeStickerDivY;


      if(activeStickerSize){
        let w = Math.max(this.activeXPos - activeStickerDiv.getBoundingClientRect().left, 30);
        activeStickerDiv.style.width = w + 'px';
        activeStickerDiv.style.height = w + 'px';
        activeStickerDiv.firstChild.firstChild.style.width = w + 'px';
        activeStickerDiv.firstChild.firstChild.style.height = w + 'px';

        return
      }
      if(activeStickerRotate){
        let center_x = (activeStickerDiv.getBoundingClientRect().left) + (activeStickerDiv.offsetHeight/2);
        let center_y = (activeStickerDiv.getBoundingClientRect().top) + (activeStickerDiv.offsetWidth/2);
        let radians = Math.atan2(x-center_x, y-center_y);
        let degree = (radians * (180 / Math.PI) * -1) + 90;
        // console.log('activeStickerRotate', radians, degree);
        activeStickerDiv.style.transform = "rotateZ("+degree+"deg)";
        return
      }

      activeStickerDiv.style.top = `${y}px`;
      activeStickerDiv.style.left = `${x}px`;
    }

    /*if(el.id === 'canvasText'){
     el.style = `top: ${x}px; left: ${y};`;
     }*/
    //e.style.top = `${x}px`;
    //e.style.left = `${y}px`;*/
    //console.log('handleOnMouseMoveBlock', el, x, y);
  };

  handleOnMouseMove = (e) => {
    let rect = this.state.canvas.getBoundingClientRect();
    let ctx = this.state.context;

    /* if(this.state.textActive && this.state.drawingText){
     let text = this.state.canvasTexts[this.state.selectedText];
     if(text){
     let dx = parseInt(e.clientX - rect.left);
     let dy = parseInt(e.clientY - rect.top);
     text.x = dx;
     text.y = dy;
     this.drawText();
     }
     }*/

    if(this.state.drawing) {

      if (this.state.brushType === 1) {
        points.push({
          x: e.clientX - rect.left,
          y: e.clientY - rect.top
        });

        ctx.strokeStyle = this.state.brushColor;
        ctx.shadowBlur = 0;
        ctx.lineWidth = this.state.lineWidth;

        var p1 = points[0];
        var p2 = points[1];

        ctx.beginPath();
        ctx.moveTo(p1.x, p1.y);

        for (var i = 1, len = points.length; i < len; i++) {
          var midPoint = midPointBtw(p1, p2);
          ctx.quadraticCurveTo(p1.x, p1.y, midPoint.x, midPoint.y);
          p1 = points[i];
          p2 = points[i + 1];
        }
        ctx.lineTo(p1.x, p1.y);
        ctx.stroke();
      }
      else if(this.state.brushType == 2) {
        ctx.shadowBlur = 0;
        mouseX = e.clientX - rect.left;
        mouseY = e.clientY - rect.top;
      }
      else if(this.state.brushType == 3) {
        points.push({
          x: e.clientX - rect.left,
          y: e.clientY - rect.top
        });

        var p11 = points[0];
        var p21 = points[1];


        ctx.strokeStyle = this.state.brushColor.replace('rgb', 'rgba').replace(')', `, ${'.5'})`);

        ctx.lineWidth = this.state.lineWidth;
        // ctx.lineJoin = ctx.lineCap = 'round';
        ctx.shadowBlur = 2;
        ctx.shadowColor = this.state.brushColor;

        ctx.beginPath();
        ctx.moveTo(p11.x, p11.y);

        for (var is = 1, lens = points.length; is < lens; is++) {
          var midPoints = midPointBtw(p11, p21);
          ctx.quadraticCurveTo(p11.x, p11.y, midPoints.x, midPoints.y);
          p11 = points[is];
          p21 = points[is + 1];
        }
        ctx.lineTo(p11.x, p11.y);
        ctx.stroke();
      }
    }

  };
  handleonMouseUpText = () => {
    let { activeTextDrawing } = this.state;
    console.log('handleonMouseUpTeext', activeTextDrawing);
    if(activeTextDrawing){
      this.setState({activeTextDiv: null,
        activeTextDivRotate: false,
        activeTextDrawing: false})
    }

  };
  handleonMouseUpBlock = (e) => {
    this.setState({
      activeTextDivRotate: false,
      activeTextDrawing: false,
      activeStickerDrop: false,
      activeStickerSize: false,
      activeStickerRotate: false
    });
  };
  handleonMouseUp = () => {
    points = [];
    this.setState({
      drawing: false,
      drawingText: false
    });
    clearInterval(this.IntervalBrushTwo);
  };
  draw = (lX, lY, cX, cY) => {
    this.state.context.strokeStyle = this.state.brushColor;
    this.state.context.lineWidth = this.state.lineWidth;
    this.state.context.moveTo(lX,lY);
    this.state.context.lineTo(cX,cY);
    this.state.context.stroke();
  };
  resetCanvas =() =>{
    let width = this.state.context.canvas.width;
    let height = this.state.context.canvas.height;
    this.state.context.clearRect(0, 0, width, height);
  };
  getDefaultStyle = () => {
    return {
      backgroundColor: '#FFFFFF',
      cursor: 'pointer'
    };
  };
  canvasStyle = () => {
    let defaults =  this.getDefaultStyle();
    let custom = this.props.canvasStyle;
    return Object.assign({}, defaults, custom);
  };
  isMobile = () => {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      return true;
    }
    return false;
  };

  renderItemContent = (item) => {
    console.log('renderItemContent', item);
    return(
      <div className='fit-content'>
        <div className='fit-item-image-container card__image'>

          <div className="card__overlay card__overlay--indigo">
            <div className="card__content">
              <div className="card__content__block">
                {/*<div className="card__content__rating">
                 <Rating item={item} />
                 </div>*/}
                <div className="card__content__title">
                  <div className="card__content__title__link">
                    <a target='_black' href={item.url} className="card__title">{item.title}</a>
                  </div>
                  <div className="card__content__title__source">
                    <div>
                      {item.wallType == 5 ? <a title={item.user.first_name} href={'/'+item.user.username}>@{item.user.username}</a> : <a href={'/'+item.site.short_url}>{item.site.short_url}</a>}
                    </div>
                  </div>
                </div>

              </div>
            </div>
            {/*<div className="card__overlay-content">

             <a target='_black' href={item.url} className="card__title">{item.title}</a>

             <ul className="card__meta card__meta--last">
             <li>
             {item.wallType == 5 ? <a title={item.user.first_name} href={'/'+item.user.username}>@{item.user.username}</a> : <a href={'/'+item.site.short_url}>{item.site.short_url}</a>}
             </li>
             <li>time</li>
             </ul>

             <div className='feed-extra-item'>
             <div className='feed-full-item'>
             1
             </div>
             <div className='feed-share-item'>
             2
             </div>
             <div className='feed-liked-item'>
             3
             </div>
             </div>
             </div>*/}
          </div>
        </div>
      </div> )
  };

  onSelectColor = (color) => {
    this.setState({brushColor: color})
  };

  saveToUndoHistory = () => {
    let { undoList, canvas } = this.state;
    let hst = undoList;
    hst.push(canvas.toDataURL());
    this.setState({undoList: hst});
  };

  resetActionHistory = (e) => {
    let that = this;
    let { undoList } = this.state;
    let reset_img = undoList.pop();
    let width = this.state.context.canvas.width;
    let height = this.state.context.canvas.height;
    points = [];

    let img = new Image();
    img.src = reset_img;
    img.onload = function() {
      that.state.context.clearRect(0, 0, width, height);
      that.state.context.drawImage(img, 0, 0, width, height, 0, 0, width, height);
    }
  };

  setDrawingText = () => {
    let { currentCanvas, canvas } = this.state;
    this.setState({currentCanvas: canvas.toDataURL()});
  };

  resetDrawingText = (e) => {
    let that = this;
    let { currentCanvas } = this.state;
    let reset_img = currentCanvas;
    let width = this.state.context.canvas.width;
    let height = this.state.context.canvas.height;

    let img = new Image();
    img.src = reset_img;
    img.onload = function() {

      that.state.context.clearRect(0, 0, width, height);
      that.state.context.drawImage(img, 0, 0, width, height, 0, 0, width, height);
    }
  };

  setBrush = (brushType) => {
    console.log('setBrush', brushType);
    if(brushType === this.state.brushType)brushType = null;
    this.setState({brushType: brushType, textActive: false,
      drawMode: brushType !== null,
      activeTextDivRotate: false,
      activeTextDivActive: false,
      activeTextDiv: false,
      activeText: false})
  };

  setStickerMode = () => {
    this.setState({brushType: null, textActive: false,
      drawMode: false,
      stickerMode: !this.state.stickerMode,
      activeTextDivRotate: false,
      activeTextDivActive: false,
      activeTextDiv: false,
      activeText: false})
  };

  onClickText = () => {
    let { activeText } = this.state;
    if(activeText){
      this.setState({activeText: false});
      return
    }
    let text = {
      text: 'Example',
      x: '200px',
      y: '200px',
      color: this.state.brushColor,
      el: null,
      fontSize: '30px'
    };

    let texts = this.state.canvasLayerTexts;
    texts.push(text);
    this.setState({canvasLayerTexts: texts, activeText: true});

    /* this.setState({brushType: null,
     toggleText: true,
     textActive: true,});
     let y = 20;
     let ctx = this.state.context;

     // get the text from the input element
     let text = {
     text: this.state.inputText,
     x: 20,
     y: y
     };

     ctx.font = "16px verdana";
     text.color = this.state.brushColor;
     text.width = ctx.measureText(text.text).width;
     text.height = 36;

     // put this new text in the texts array.push(text);
     let texts = this.state.canvasTexts;
     texts.push(text);
     this.setState({canvasTexts: texts});

     // redraw everything
     //draw();
     this.setDrawingText();
     this.drawText(true);*/
  };

  drawText = (first) => {
    let ctx = this.state.context;
    //if(!first){
    this.resetDrawingText();
    //
    // }
    this.state.canvasTexts.forEach(text=> {
      ctx.fillText(text.text, text.x, text.y);
    });
  };

  onInputText = (e) => {
    console.log('onInputText', e);
    this.setState({inputText: e.target.value})
  };

  onInputTextDiv = (e, i) => {
    let val = e.target.value;
    let selectText = this.state.canvasLayerTexts[i];
    let arr = this.state.canvasLayerTexts.map((item, z) => {
      if(z===i){
        item.text = val
      }
      return item
    });
    this.setState({canvasLayerTexts: arr});
    console.log('onInputTextDiv', i, val, selectText);

  };

  handleOnMouseDownBlock = (e) => {
    let { activeTextDivTyping, activeStickerDrop } = this.state;
    if(activeTextDivTyping && !e.target.classList.contains('cr_text__textarea')){
      this.setState({activeTextDivTyping : false, activeTextDivActive: false});
    }
    if(e.target.classList.contains('cr_text__selection_handler')){
      this.setState({activeStickerSize: true, activeStickerRotate: false});
    }
    if(e.target.classList.contains('cr_text__selection_rotate')){
      this.setState({activeStickerRotate: true, activeStickerSize: false});
    }
    ///console.log('handleOnMouseDownBlock', activeTextDivTyping, e.target)
  };

  renderText = (text) => {
    let texts = [];
    text.split('\n').forEach((item, i) => {
      texts.push(item);
      if(i !== text.split('\n').length-1){
        texts.push(this.renderBr())
      }
    });
    return texts
  };

  renderBr = () => {
    return(<br/>)
  };

  onClickSticker = (item) => {
    let { activeStickers } = this.state;
    let activeStickersT = activeStickers;
    item.uuid = uuid.v4();
    activeStickersT.push(item);
    this.setState({activeStickers, activeStickersT,
      activeStickerItem: null,
      activeSticker: true
    });
  };

  onClickStickerView = (item) => {
    console.log('onClickStickerView', item);

  };

  handleOnMouseDownSticker = (e, item) => {
    let cTarget = e.currentTarget;

    this.activeStickerDivX = this.activeXPos - cTarget.offsetLeft;
    this.activeStickerDivY = this.activeYPos - cTarget.offsetTop;

    this.setState({activeStickerItem: item,
      activeStickerDrop: true,
      activeStickerDiv: cTarget});
  };

  handleOnMouseUpSticker = (e, item) => {
    this.setState({activeStickerDrop: false,
      activeStickerSize: false,
      activeStickerRotate: false});
  };

  render() {

    let { item, user, styleImg, widthBlock, grid , heightBlock} = this.props;
    let { toggleText, drawing, activeTextDivTyping,
      stickerMode, stickers, activeStickers,
      activeStickerItem, activeSticker,
      drawMode, activeText, activeTextDiv, activeTextDivActive,
      inputText, canvasLayerTexts } = this.state;
    // console.log('Item2', item.img);
    let {width, height} = item.arcImage(600, 300);
    //console.log('Itme7', width, height);
    let styleImgBase = styleImg;
    let styleItem = styleImg ? styleImg : {width: `${width}px`, height: `${height}px`};
    //if(!grid){styleItem['backgroundImage'] = `url(${item.img})`;}
    if(heightBlock){styleImg.height = heightBlock}
    if(widthBlock){styleImg.width = widthBlock}
    if(grid){delete styleItem['top'];delete styleItem['left'];}




    return (
      <div style={styleImg} className="share_item">
        {/*<canvas className="canvas" id="canvas" width={width} height={height} />*/}
        <div className="share_item__row_base">
          <div className="share_item__left_b_run">
            {<ColorPicker onSelectColor={this.onSelectColor} id='yourPicker'/>}
          </div>
          <div className="share_item__center">
            <div className="share_item__center_top">
              <div className="share_item__center_top_left">
                <div className="share_item reset_btn" onClick={this.resetActionHistory}></div>
              </div>
              <div className="share_item__center_top_right">
                {<div className="share_item brush_btn" onClick={this.onClickText}>Text</div>}
                <div className="share_item brush_btn" onClick={(tp) => this.setBrush(1)}>1</div>
                <div className="share_item brush_btn" onClick={(tp) => this.setBrush(2)}>2</div>
                <div className="share_item brush_btn" onClick={(tp) => this.setBrush(3)}>3</div>
                <div className="share_item brush_btn" onClick={(tp) => this.setStickerMode()}>sticker</div>
              </div>
            </div>

            <div

              onMouseUp = {this.handleonMouseUpBlock}
              onTouchEnd = {this.handleonMouseUpBlock}
              className="canvas_block">

              <canvas style={{pointerEvents: drawMode ? 'all': 'none'}}
                      className="canvas" id="canvas" width={width} height={height}
                      onMouseDown = {this.handleOnMouseDown}
                      onTouchStart = {this.handleOnMouseDown}
                      onMouseMove = {this.handleOnMouseMove}
                      onTouchMove = {this.handleOnMouseMove}
                      onMouseUp = {this.handleonMouseUp}
                      onTouchEnd = {this.handleonMouseUp}
              >
              </canvas>

              <div style={{pointerEvents: !drawMode ? 'all': 'none'}}
                   className="layer__text">

                {canvasLayerTexts.map((text, i) => {
                  return(
                    <div
                      key={i}
                      style={{top: text.y, left: text.x,
                        fontSize: text.fontSize,
                        color: text.color}}
                      onMouseDown = {this.handleOnMouseDownText}
                      onTouchStart = {this.handleOnMouseDownText}
                      onMouseUp = {this.handleonMouseUpText}
                      onTouchEnd = {this.handleonMouseUpText}
                      id="canvasText"
                      className={activeTextDivTyping ? "cr_text active" : "cr_text"}>

                      {activeTextDivTyping ?
                        <textarea
                          onInput={(e, z) => this.onInputTextDiv(e, i)}
                          style={{fontSize: text.fontSize,
                            color: text.color}}
                          className="cr_text__textarea"
                          value={text.text}/>
                        :
                        <div style={{fontSize: text.fontSize,
                          color: text.color}} className="cr_text__inner">{this.renderText(text.text)}</div>
                      }

                      {activeTextDivActive && !activeTextDivTyping ?
                        <div  className="cr_text__selection">
                          <div className="cr_text__selection_handler" id="pe_nw" />
                          <div className="cr_text__selection_handler" id="pe_ne" />
                          <div className="cr_text__selection_handler" id="pe_se" />
                          <div className="cr_text__selection_handler" id="pe_sw" />
                        </div>
                        : null}

                    </div>
                  )
                })}

              </div>

              <div style={{pointerEvents: !drawMode ? 'all': 'none'}}
                   className="layer__sticker">
                {activeStickers.map((item, i) => {
                  return(
                    <div
                      key={item.uuid}
                      onClick={(it) => this.onClickStickerView(item)}
                      onMouseDown = {(e, it) => this.handleOnMouseDownSticker(e, item)}
                      onTouchStart = {(e, it) => this.handleOnMouseDownSticker(e, item)}
                      onMouseUp = {(e, it) => this.handleOnMouseUpSticker(e, item)}
                      onTouchEnd = {(e, it) => this.handleOnMouseUpSticker(e, item)}
                      className={`cr_sticker ${(activeStickerItem === item ? ' active ' : null)}`}
                    >
                      <div className="cr_sticker__inner">
                        <img className="cr_sticker__img" src={item.path}/>
                      </div>
                      {activeStickerItem === item ?
                        <div  className="cr_text__selection">
                          {/*<div className="cr_text__selection_handler" id="pe_nw" />*/}
                          {/*<div className="cr_text__selection_handler" id="pe_ne" />*/}

                          <div className="cr_text__selection_handler" id="pe_se" />
                          <div className="cr_text__selection_rotate" id="pe_rt" />
                          {/*<div className="cr_text__selection_handler" id="pe_sw" />*/}
                        </div>
                        : null}
                    </div>
                  )
                })}
              </div>
            </div>

            <div className="share_item__center_bottom">
              {activeText ?
                <div className="cr_text__bottom">
                  sd
                </div>
                : null}

              {stickerMode ?
                <div className="sticker_block__bottom">
                  1
                  <div  className="sticker_block__stickers">
                    {stickers.map(item=>{
                      return(
                        <div onClick={(it) => this.onClickSticker(item)} key={item.id} className="sticker_block__sticker">
                          <img className="sticker_block__sticker_img" src={item.path}/>
                        </div>
                      )
                    })}
                  </div>
                </div>
                : null}

              {/*<input onInput={this.onInputText} value={inputText}/>*/}
            </div>
          </div>
          <div>
            r
          </div>
        </div>
        {/* <Stage className="canvas" id="canvas" width={width} height={height}>
         <Layer>
         <Image image={image} />
         </Layer>
         </Stage>*/}
      </div>
    );
  }
}
/*<div className="item share_item">
 <div className="item__card card" style={styleItem}>
 share
 {this.renderItemContent(item)}
 </div>
 </div>*/