import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';

@inject("appState")
@observer
export default class Loader extends React.Component {

  render() {
    let { color, style } = this.props;
    return (
      <div style={{'border-top-color': color, ...style}} className="loader">

      </div>
    );
  }
}