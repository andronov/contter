import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';

@inject("appState")
@observer
export default class Switcher extends React.Component {

  onClickSwitch = (e) => {
    let elem = e.currentTarget;
    let action = !elem.firstChild.firstChild.checked;
    //elem.firstChild.firstChild.checked = action;
    this.props.onChangeSwitcher(action, this.props.type);
    console.log('onClickSwitch.', elem)
  };

  render() {
    let { labelText, active } = this.props;

    return (
      <div onClick={this.onClickSwitch} className='switcher'>
        <div className='switcher_block'>
          <input checked={active} className="switcher__input" type="checkbox" id="s5" />
          <label className="switcher__v3" for="s5"/>
        </div>
        <div style={{fontWeight: (active ? '600': 'normal')}} className="switcher__label">
          {labelText}
        </div>
      </div>
    );
  }
}