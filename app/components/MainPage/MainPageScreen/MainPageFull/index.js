import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import { Facebook, Twitter } from '../../../../lib/react-jwt-auth';

const handleSocialLogin = (user, err) => {
  console.log(user);
  console.log(err)
};

const authConfig = {
  // --- DEFAULTS ---
  // tokenName: 'if-token',
  // authHeader: 'Authorization',
  // authToken: 'Bearer',
  // baseUrl: '/',
  // loginUrl: 'auth/login',
  // signupUrl: 'auth/signup',
  // refreshUrl: 'auth/refresh',
  // oauthUrl: 'auth/{provider}', // dynamic
  // profileUrl: 'me'
  // --- REQUIRED ---
  baseUrl:"http://localhost:8080/api/"
};


@inject("appState")
@observer
export default class MainPageFull extends React.Component {

  render() {
    return (
      <div>

        <Twitter clientId="1338796609520757" />
        <Facebook clientId="1338796609520757" />
        {/*<Facebook clientId="1420398708283143" />*/}
        {/*<SocialLogin provider='facebook' appId='1420398708283143' callback={handleSocialLogin}>
          <button>Login with Facebook</button>
        </SocialLogin>*/}
        MainPageFull
      </div>
    );
  }
}
