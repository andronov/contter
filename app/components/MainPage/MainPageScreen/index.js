import * as React from 'react';
import { observer , inject} from 'mobx-react';
import Screen from '../Screen';
import MainPageFull from './MainPageFull';

@inject("appState")
@observer
export default class MainPageScreen extends React.Component {

  render() {
    const { user } = this.props;
    console.log(' MainPageScreen', user && user.id);
    return (
      <Screen header={<div>MainPageScreen</div>} children={
        <MainPageFull {...this.props}/>
      }>
      </Screen>
    );
  }
}
