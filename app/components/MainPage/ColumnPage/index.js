import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import Screen from '../Screen';
import ColumnView from './ColumnView';

@inject("appState")
@observer
export default class ProfilePage extends React.Component {

  componentDidMount = () => {
  };

  render() {
    const { user, appState} = this.props;

    return (
      <Screen header={null} children={
        <ColumnView user={user} />
      }>
      </Screen>
    );
  }
}