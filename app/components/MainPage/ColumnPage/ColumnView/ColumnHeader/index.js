import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';

@inject("appState")
@observer
export default class ColumnHeader extends React.Component {

  render() {
    let { wall, appState } = this.props;
    //console.log('Column', this);
    let color = appState.get_color(1, wall);
    let colorOpacity = appState.get_color('0.20', wall);
    let backOpacity = appState.get_color( '0.07', wall);

    return (
      <div style={{borderColor: color}} className="cl__header">
        {/*<div style={{background: color}} className="cl__header__top"></div>*/}
        <div className="cl__header__inner">
          <div className="cl__header__content">
            <div className="cl__header__content_title">{wall.name} <span>/{wall.slug}</span></div>
          </div>
        </div>
        <div style={{background: color}} className="cl__header__bottom"></div>
      </div>
    );
  }
}