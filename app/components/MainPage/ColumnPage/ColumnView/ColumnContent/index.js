import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import ColumnItem from '../ColumnItem';
import { Scrollbars } from 'react-custom-scrollbars';

@inject("appState")
@observer
export default class ColumnContent extends React.Component {

  render() {
    let { wall, appState } = this.props;
    let color = appState.get_color(1, wall);
    let colorOpacity = appState.get_color('0.20', wall);
    let backOpacity = appState.get_color( '0.07', wall);

    return (
      <div className="cl__content">
        <Scrollbars style={{ width: 'auto', height: '100%', zIndex: 5}}>
        {wall.items.map(item => {
          return(<ColumnItem key={item.id} item={item} />)
        })}
        </Scrollbars>
      </div>
    );
  }
}