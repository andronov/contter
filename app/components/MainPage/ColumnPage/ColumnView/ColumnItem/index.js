import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import Item from '../../../Item';

@inject("appState")
@observer
export default class ColumnItem extends React.Component {

  render() {
    let { item, appState } = this.props;
    console.log('ColumnItem', item);
    let width = 400;

    return (
      <div className="cl__item">
        <Item maxWidth={width} user={appState.currentUser} item={item}/>
      </div>
    );
  }
}