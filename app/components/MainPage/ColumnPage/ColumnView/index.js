import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import Sortable from 'react-anything-sortable';
import Column from './Column';
import ColumnHeader from './ColumnHeader';
import ColumnContent from './ColumnContent';
import { Scrollbars } from 'react-custom-scrollbars';


@inject("appState")
@observer
export default class ColumnView extends React.Component {

  componentDidMount = () => {
    const { user, appState} = this.props;
    if(!user)return;
    let walls = user.walls;
    walls.forEach(wall => {
      appState.currentUser.loadItems(wall.slug, 0, 3)
    })
  };

  render() {
    const { user, appState} = this.props;
    if(!user) return(<div></div>);

    let walls = user.walls;

//<Scrollbars style={{ width: 'auto', height: '80%', zIndex: 10}}></Scrollbars>
    return (
      <div className="columns">
        <div className="columns__inner">
          <div className="columns__block" style={{ width: 'auto', height: '100%'}}>
            <div className="columns__header" >
              <div>{'<-'} return</div>
            </div>
            <Scrollbars style={{ width: 'auto', height: '100%', zIndex: 10}}>
              <div style={{ width: `${walls.length*500}px`, height: '100%', position: 'relative'}}>
                {walls.map(wall => {
                  return (<Column className="column" wall={wall} key={wall.id}> <ColumnHeader wall={wall} /><ColumnContent wall={wall} /></Column>)
                })}
              </div>
            </Scrollbars>
          </div>
        </div>
      </div>
    );
  }
}