import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';

import { sortable } from 'react-anything-sortable';


@inject("appState")
@observer
export default class Column extends React.Component {
  render() {
    //console.log('Column', this);
    return (
      <div
        className={this.props.className}
        style={{width: '400px'}}
        onMouseDown={this.props.onMouseDown}
        onTouchStart={this.props.onTouchStart}
      >
        {this.props.children}
      </div>
    );
  }
}