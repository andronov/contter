import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';

import MainPageScreen from './MainPageScreen';
import WallScreen from './WallScreen';
import {FAVICON_BASE64} from 'services/constants';
import {blinkTitleStop} from 'services/utils';
import ProfilePage from './ProfilePage';
import AddEditWall from './AddEditWall';
import ColumnPage from './ColumnPage';
import FollowersPage from './FollowersPage';

@inject("appState")
@observer
export default class MainPage extends React.Component {

  componentWillMount() {
    document.addEventListener('visibilitychange', this.visibilityChange);
    let { appState } = this.props;

    console.log('appState',  appState);
    if(appState.router.history.location.pathname.endsWith('followers')){
      appState.app_content = 'followers';
    }
  }

  componentWillUnmount() {
    document.removeEventListener('visibilitychange', this.visibilityChange);
  }

  componentWillReceiveProps(nextProps) {
  }

	/**
	 * Меняем favicon,ставим стандартный если сообщение новое
	 * есть только в одном чате
	 */
  visibilityChange = () => {
    /*if (document.visibilityState == 'visible') {
      window.playSoundNotify = false;
      blinkTitleStop();
      if (appState.currentUser && !appState.currentUser.userThreads.filter(th => { return th.thread.newMessage; }).length) {
        document.getElementById('favicon').href = FAVICON_BASE64;
      }
    }*/
  };



  getScreen = (name) => {
    const { user, matches} = this.props;
    if(!user)return(<div></div>);

    switch (name) {
      case 'main':
        return (
          <MainPageScreen user={user}/>
        );
        break;
      case 'wall':
        return (
        <WallScreen matches={matches} user={user} />
        );
        break;
      case 'add_wall':
        return (
          <AddEditWall matches={matches} user={user} />
        );
        break;
      case 'profile':
        return (
          <ProfilePage matches={matches} user={user} />
        );
        break;
      case 'column':
        return (
          <ColumnPage matches={matches} user={user} />
        );
        break;
      case 'followers':
        return (
          <FollowersPage matches={matches} user={user} />
        );
        break;
      default:
        return (
          <div>Empty</div>
        );
    }
  };

  render() {
    let {appState} = this.props;

    return (
      <div className='page__layout'>
          <div className='page__content'>
            {this.getScreen(appState.app_content)}
          </div>
      </div>
    );
  }
}
