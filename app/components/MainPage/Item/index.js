import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import Rating from './Rating';
import Link from '../../Link';
import Heart from './Heart';

@inject("appState")
@observer
export default class Item extends React.Component {

  Share =(item) => {
    let { appState } = this.props;
    console.log('Share', item);
    appState.update({shareItem: item, shareOpen: true});
    this.props.history.push('/share');

  };

  Like = (item) => {
    console.log('Like', item);
    item.update({liked: !item.liked,
      liked_count: !item.liked ?
        item.liked_count+1 : item.liked_count-1});
    item.setLike();
  };

  Rating = (item, type, action) => {
    let { rating } = item;

    item.setRating(type, action);
    //console.log('Rating', item, type, action);
  };

  renderItemContent = (item) => {
    return(
      <div className='fit-content'>
        <div className='fit-item-image-container card__image'>

          <div className="card__overlay card__overlay--indigo">
            <div className="card__content">
              <div className="card__content__block">
                {/*<div className="card__content__rating">
                  <Rating item={item} />
                </div>*/}
                <div className="card__content__title">
                  <div className="card__content__title__link">
                    <a target='_black' href={item.url} className="card__title">{item.title}</a>
                  </div>
                  <div className="card__content__title__source">

                    <div className="card__content__title__source_top">
                      <div>
                      {item.wallType !== 5 ? <Link type="profile" style={{textDecoration: 'none'}}
                                                   href={`/${item.site.slug}`} value={item.site.short_url} to={`/${item.site.slug}`} {...this.props}/> : null}
                      {item.wallType == 5 ?
                        <div>
                          <a title={item.user.first_name} href={'/'+item.user.username}>@{item.user.username}</a>
                          {item.site.short_url ? ' from ' : ''}
                          {item.site.short_url ?
                          <Link type="profile" style={{textDecoration: 'none'}}
                                href={`/${item.site.slug}`} value={item.site.short_url} to={`/${item.site.slug}`} {...this.props}/>
                            : null}
                        </div>
                        : null}
                      </div>
                      <div className="card__content__time">
                        {item.fromNow() }
                        </div>
                      <div className="card__content__rating">
                        <Rating Rating={this.Rating} item={item} />
                      </div>
                    </div>

                    <div className="card__content__title__source_bottom">
                      <div className="card__content__button">
                        <div onClick={(it) => this.Share(item)} className="card__content__share">
                          Share +15
                        </div>

                        <Heart Like={this.Like} item={item} {...this.props}/>

                        <div className="card__content__more">
                          <div className="card__content__more_inner">
                            <div />
                            <div />
                            <div />
                          </div>
                        </div>

                      </div>
                    </div>

                  </div>
                </div>

              </div>
            </div>
            {/*<div className="card__overlay-content">

              <a target='_black' href={item.url} className="card__title">{item.title}</a>

              <ul className="card__meta card__meta--last">
                <li>
                  {item.wallType == 5 ? <a title={item.user.first_name} href={'/'+item.user.username}>@{item.user.username}</a> : <a href={'/'+item.site.short_url}>{item.site.short_url}</a>}
                </li>
                <li>time</li>
              </ul>

              <div className='feed-extra-item'>
                <div className='feed-full-item'>
                  1
                </div>
                <div className='feed-share-item'>
                  2
                </div>
                <div className='feed-liked-item'>
                  3
                </div>
              </div>
            </div>*/}
          </div>
        </div>
      </div> )
  };

  render() {
    let { item, user, styleImg, maxWidth, widthBlock, grid , heightBlock} = this.props;
    let {width, height} = item.arcImage(maxWidth ? maxWidth : 600, 1200);
    let styleImgBase = styleImg;
    let styleItem = styleImg ? styleImg : {width: `${width}px`, height: `${height}px`};
    if(!grid){styleItem['backgroundImage'] = `url(${item.img})`;}
    if(heightBlock){styleImg.height = heightBlock}
    if(widthBlock){styleImg.width = widthBlock}
    if(grid){delete styleItem['top'];delete styleItem['left'];}

    return (
      <div className="item">
        <div className="item__card card" style={styleItem}>
          {grid ? <img className="card__image__grid" style={styleImgBase} src={item.img} /> : null}
          {this.renderItemContent(item)}
        </div>
      </div>
    );
  }
}