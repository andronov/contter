import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';

@inject("appState")
@observer
export default class Rating extends React.Component {

  onMouseEnter = (e, up) => {
    let { user, appState} = this.props;
    let color = appState.get_color(1);
    if((up && this.checkType('up')) || (!up && this.checkType('down')))return;
    if(up){
      e.target.style = `border-left: 6px solid transparent;border-right: 6px solid transparent;border-bottom: 6px solid ${color};`;
    }
    else{
      e.target.style = `border-left: 6px solid transparent;border-right: 6px solid transparent; border-top: 6px solid ${color};`;
    }
  };

  onMouseLeave = (e, up) => {
    let { user, appState} = this.props;
    let color = appState.get_color(1);
    if((up && this.checkType('up')) || (!up && this.checkType('down')))return;
    if(up){
      e.target.style = 'border-left: 6px solid transparent;border-right: 6px solid transparent;border-bottom: 6px solid #ffffff;';
    }
    else{
      e.target.style = `border-left: 6px solid transparent;border-right: 6px solid transparent; border-top: 6px solid #ffffff;`;
    }
  };

  checkType = (type) => {
    let { item } = this.props;
    let { rating } = item;
    return rating.your === type;

  };

  onClickRating = (e, type, action) => {
    let { Rating, item } = this.props;
    Rating(item, type, action)
  };

  render() {
    let { item, appState } = this.props;
    let { rating } = item;
    let color = appState.get_color(1);

    return (
      <div className="rating">
        <div className="rating__inner">
          <div onClick={(e, up, ac) => this.onClickRating(e, 'up', this.checkType('up') ? 'unvoted': 'up')}
               onMouseLeave={(e, up) => this.onMouseLeave(e, true)}
               onMouseEnter={(e, up) => this.onMouseEnter(e, true)}
               className={"rating__arrow up" + (this.checkType('up') ? ' active ': '')}>
            <span style={{borderBottom: this.checkType('up') ? `6px solid ${color}` : ''}}/>


          </div>
          <div className="rating__count">{rating.score}</div>
          <div onClick={(e, up, ac) => this.onClickRating(e, 'down', this.checkType('down') ? 'unvoted': 'down')}
               onMouseLeave={(e, up) => this.onMouseLeave(e, false)}
               onMouseEnter={(e, up) => this.onMouseEnter(e, false)}
               className={"rating__arrow down" + (this.checkType('down') ? ' active ': '')}>
            <span style={{borderTop: this.checkType('down') ? `6px solid ${color}` : ''}}/>

          </div>
        </div>
      </div>
    );
  }
}