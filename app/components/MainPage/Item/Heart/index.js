import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';

@inject("appState")
@observer
export default class Heart extends React.Component {

  onClickHeart = (e) => {
    let { appState, item, Like } = this.props;
    //let color = appState.color;

    Like(item);
    if(!item.liked)return;

    let elem = e.currentTarget.parentNode.nextSibling.nextSibling;
    let b = Math.floor((Math.random() * 100) + 1);
    let d = ["flowOne", "flowTwo", "flowThree"];
    let a = ["colOne", "colTwo", "colThree", "colFour", "colFive", "colSix"];
    let c = (Math.random() * (1.6 - 1.2) + 1.2).toFixed(1);
    let heart = document.createElement('div');
    heart.className = `heart part-${b} ${a[Math.floor((Math.random() * 6))]}`;
    heart.style = 'font-size: ' + Math.floor(Math.random() * (50 - 22) + 22) + 'px;';
    heart.innerHTML = '<i class="heart_icon active"></i>';
    elem.appendChild(heart);
    heart.style = 'animation: '+ d[Math.floor((Math.random() * 3))] +' '+ c +'s linear;';
    setTimeout(function() {
      elem.childNodes.forEach(node=>{
        if(node.classList.contains('part-'+b)){
          elem.removeChild(node)
        }
      });
    }, c * 900)
  };

  onMouseEnter = (e) => {
    let elem = e.currentTarget;
    elem.parentNode.nextSibling.nextSibling.style = 'display: block;'
  };

  onMouseLeave = (e) => {
    let elem = e.currentTarget;
    elem.parentNode.nextSibling.nextSibling.style = 'display: none;'
  };

  render() {
    let { item} = this.props;

    return (
      <div className="card__content__like">
        <div className="card__content__like_bl">
          <div
          onMouseEnter={this.onMouseEnter}
          onMouseLeave={this.onMouseLeave}
          onClick={this.onClickHeart}
          className="card__content__like_btn" >
          <div className={"heart_icon " + (item.liked ? 'active': '')} />
          </div>
        </div>
        <div className="card__content__like_score">
          {item.liked_count ? `+${item.liked_count}`: ''}
        </div>
        <div  className="hearts">

        </div>
      </div>
    );
  }
}