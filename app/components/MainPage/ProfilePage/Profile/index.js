import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import Link from '../../../Link';
import Img from '../../Img';
import { Scrollbars } from 'react-custom-scrollbars';

@inject("appState")
@observer
export default class Profile extends React.Component {

  state = {
    openFollowBlock: false,
  };

  onClickFollowBlock = () => {
    this.setState({ openFollowBlock: !this.state.openFollowBlock })
  };

  renderAlienBottom = (colorOpacity, color, us) => {
    let { openFollowBlock } = this.state;
    return(
      <div style={{borderColor:  colorOpacity,
        borderTopWidth: '1px',
        borderTopStyle: 'solid'}}
           className="lf_column__item pf_column__footer no-select lf_column__item__footer">
        <div className="lf_column__item_inner">
          <div className="pf_column__footer_fl">
            <div style={{color: '#a2a6aa', cursor: 'default'}}>12</div>
          </div>
          <div className="pf_column__footer_st">
            <span style={{color: color, textDecoration: 'none'}}
                  onClick={this.onClickFollowBlock}>{openFollowBlock ? 'CLOSE': "FOLLOW"}</span>
          </div>
        </div>
      </div>
    )
  };

  onMouseEnterFollow = (e) => {
    let { color } = this.props;
    if(e.target.classList.contains('active')){
      e.target.lastChild.firstChild.style = '';
    }
    else{
      e.target.lastChild.firstChild.style = `background:${color}`;
    }
  };

  onMouseLeaveFollow = (e) => {
    let { color } = this.props;
    if(!e.target.classList.contains('active')){
      e.target.lastChild.firstChild.style = '';
    }
    else{
      e.target.lastChild.firstChild.style = `background:${color}`;
    }

  };

  onClickFollow = (wall) => {
    console.log('onClickFollow', wall, this.props.username);
    wall.followUser(this.props.username);
  };

  onClickAlienWall = (wall) => {
    console.log('onClickAlienWall', wall);
  };

  renderAlienWalls = (user) => {
    let { profileUser, colorOpacity, backOpacity,
      color, appState} = this.props;
    let us = profileUser;
    let slugwall = this.props.match.params.slugwall;
    return user.walls.map((item, i) => {
      return (

        <div onClick={(ti) => this.onClickAlienWall(item)}
             key={i}
             className={"lf_column__item hovered" + (slugwall && slugwall === item.slug ? ' active ': '')}>
          <Link type="profile_wall" style={{ textDecoration: 'none'}}
                href={`/${us.username}/!/${item.slug}`}
                value={
                  <div className="lf_column__item_inner">
                    <h2 className="lf_column__item__title">{item.name}</h2>
                    <p className="lf_column__item__desc">/{item.slug}</p>
                    <span className="lf_column__line" style={{background: color}} />
                  </div>
                }
                to={`/${us.username}/!/${item.slug}`} {...this.props}>
          </Link>
        </div>
      )
    });
  };

  renderSuggestUsers = (user) => {
    let { profileUser, colorOpacity, backOpacity,
      color, appState} = this.props;
    console.log('renderSuggestUsers', user.suggestUsers);
    /*
     * <Link type="followers" style={{color: '#a2a6aa', textDecoration: 'none'}}
     href={`/${us.username}/followers`} value="12" to={`/${us.username}/followers`} {...this.props}/>
     * */
    return user.suggestUsers.map((item, i) => {
      return (

        <div
             key={item.id}
             className={"lf_column__item hovered"}>
          <Link type="profile_wall" style={{ textDecoration: 'none'}}
                href={`/${item.username}`}
                value={
                  <div className="lf_column__item_inner">
                    <h2 className="lf_column__item__title">{item.first_name}</h2>
                    <p className="lf_column__item__desc">/{item.username}</p>
                    <span className="lf_column__line" style={{background: color}} />
                  </div>
                }
                to={`/${item.username}`} {...this.props}>
          </Link>
        </div>
      )
    });
  };

  render() {
    let { profileUser, colorOpacity, backOpacity,
      color, appState} = this.props;
    let { openFollowBlock } = this.state;
    console.log('PprofileUser3', profileUser);
    let user = appState.currentUser;
    // TODO Обработать ошибку если нет юзера
    if(!profileUser){return(<div>none user</div>)}
    let us = profileUser;
    let alien = true;
    let is_site = us.is_site;
    if(us && us.id === user.id){alien = false}
    // TODO для ссылки сделать красиво

    let user_walls = user.walls.map((wall) => {
      wall.in_wall = false;

      if(wall.originalSources.users && !us.is_site){
        if(wall.originalSources.users.indexOf(us.id) !== -1){
          wall.in_wall = true
        }
      }
      if(wall.originalSources.sites && us.is_site){
        if(wall.originalSources.sites.indexOf(us.id) !== -1){
          wall.in_wall = true
        }
      }
      return wall
    });
    return (
      <div className="lf_column_f pf_column_f">
      <div className="lf_column pf_column" style={{borderColor: colorOpacity}}>
        <div className="pf_column__top">
          <div className="pf_column__top__title">{us.first_name}</div>
          <div className="pf_column__top__username">
            <Link type='profile' style={{color: color, textDecoration: 'none'}}
                  href={`/${us.username}`}
                  value={!us.is_site ? '@' + us.username : "/"+us.username}
                  to={`/${us.username}`} {...this.props}/>
            </div>
        </div>

        <div className="pf_column__avatar">
          <Img type="profile" style={{width: '190px', marginLeft: '-1px'}}
               src={us.get_ava}/>
        </div>

        <div className="pf_column__info">
          <div className="pf_column__info__title">{us.description}</div>
          {us.website ? <div className="pf_column__info__site"><a target="blank" href={us.website}>{us.website}</a></div> : null}
        </div>

        {openFollowBlock && alien ?
          <div className="pf_column__follow" style={{borderColor:  colorOpacity,
            borderTopWidth: '1px',
            borderTopStyle: 'solid'}}>
            <div className="pf_column__follow_p">Move to</div>
            <div className="pf_column__follow_block">
              <Scrollbars style={{ width: '100%', height: '200px'}}>
            {user_walls.map(wall => {
              return(
                <div
                  onClick={(it) => this.onClickFollow(wall)}
                  onMouseLeave={this.onMouseLeaveFollow}
                  onMouseEnter={this.onMouseEnterFollow} className={`pf_column__follow_item ${wall.in_wall ? 'active': ''}`}>
                  <div className="pf_column__follow_title">{wall.name}</div>
                  <div className="pf_column__follow_point">
                    <div style={wall.in_wall ? {'background': color}: {}} className="pf_column__follow_point_p"></div>
                  </div>
                </div>
              )
            })}
                </Scrollbars>
            </div>
          </div>
          : null}

        {
          !alien
            ?
            <div style={{borderColor:  colorOpacity,
                         borderTopWidth: '1px',
                         borderTopStyle: 'solid'}}
                 className="lf_column__item pf_column__footer no-select lf_column__item__footer">
              <div className="lf_column__item_inner">
                <div className="pf_column__footer_fl">
                  <Link type="followers" style={{color: '#a2a6aa', textDecoration: 'none'}}
                      href={`/${us.username}/followers`}
                        value={us.followersCount>0 ? us.followersCount : ''}
                        to={`/${us.username}/followers`} {...this.props}/>
                </div>
                <div className="pf_column__footer_st">
                  <Link type="settings" style={{color: color, textDecoration: 'none'}}
                      href={`/${us.username}/settings`} value="SETTINGS" to={`/${us.username}/settings`} {...this.props}/>
                </div>
              </div>
            </div>
            :
            this.renderAlienBottom(colorOpacity, color, us)
        }
      </div>

        {alien && !is_site && profileUser.walls.length ?
          <div className="lf_column pf_column"
               style={{borderColor: colorOpacity,
                 marginTop: '25px'}}>
            <div style={{backgroundColor: backOpacity,
              borderColor:  colorOpacity,
              borderBottomWidth: '1px',
              borderBottomStyle: 'solid'}} className="lf_column__item lf_column__item__header">
              <div className="lf_column__item_inner">
                <h2 className="lf_column__item__title"></h2>
                <p className="lf_column__item__desc">Profile's walls</p>
              </div>
            </div>
            {this.renderAlienWalls(profileUser)}
          </div>
            :
            null
        }

        {alien && is_site && profileUser.suggestUsers.length ?
          <div className="lf_column pf_column"
               style={{borderColor: colorOpacity,
                 marginTop: '25px'}}>
            <div style={{backgroundColor: backOpacity,
              borderColor:  colorOpacity,
              borderBottomWidth: '1px',
              borderBottomStyle: 'solid'}} className="lf_column__item lf_column__item__header">
              <div className="lf_column__item_inner">
                <h2 className="lf_column__item__title"></h2>
                <p className="lf_column__item__desc">Similar profiles</p>
              </div>
            </div>
            <div style={{maxHeight: '250px', height: (profileUser.suggestUsers.length*60>250 ? '250px': `${profileUser.suggestUsers.length*60}px`)}}
                 className="lf_column__suggest">
              <Scrollbars width="100%" height="100%">
              {this.renderSuggestUsers(profileUser)}
              </Scrollbars>
            </div>

          </div>
          :
          null
        }

      </div>
    );
  }
}