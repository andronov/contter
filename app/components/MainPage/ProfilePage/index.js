import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import Screen from '../Screen';
import Header from '../Header';
import Profile from './Profile';
import { Scrollbars } from 'react-custom-scrollbars';
import FeedItem from '../FeedItem';
import LazyLoading from '../LazyLoading';

@inject("appState")
@observer
export default class ProfilePage extends React.Component {

  componentDidMount = () => {
    // TODO сделать красиво и куда то перенести ?
    let { appState } = this.props;
    console.log('componentDidMount', this);
    /// TODO сделать проверку на наличия юзера;
    let username = this.props.match.params.username;
    let slugwall = this.props.match.params.slugwall;
    if(this.props.match.params.slug){
      username += '/' + this.props.match.params.slug;
    }
    let us = appState.users.filter(us => us.username === username);
    let profileUser = us.length && us[0];
    if(!profileUser){
      appState.loadingProfile = true;
      appState.getUser(this.props.match.params.username + (
          this.props.match.params.slug ? '/'+this.props.match.params.slug: ''
        )
      );
      return
    }else{
      appState.loadingProfile = false;
    }

    profileUser.loadItems(slugwall ? slugwall : '#', 0, 10);
    if(appState.currentUser !== profileUser){
      profileUser.loadAllUserWalls();
      if(profileUser.is_site){
        profileUser.loadSuggestUsers();
      }
    }

  };

  render() {
    const { appState} = this.props;
    let { loadingProfile } = appState;
    let user = appState.currentUser;
    console.log('ProfilePage', this);
    let username = this.props.match.params.username;
    let slugwall = this.props.match.params.slugwall;
    if(this.props.match.params.slug){
      username += '/' + this.props.match.params.slug;
    }

    let us = appState.users.filter(us => us.username === username);
    let profileUser = us.length && us[0];
    let color = appState.get_color(1, profileUser);
    let colorOpacity = appState.get_color('0.20', profileUser);
    let backOpacity = appState.get_color( '0.07', profileUser);
    let items = profileUser && profileUser.items.length ? profileUser.items : [];
    let globalItems = items;
    //console.log('ProfilePagesds', slugwall, profileUser, items);
    if(profileUser && !profileUser.is_site){
      items = items.filter(it => it.wallType === 5).sort((a,b)=> a.created - b.created).reverse();
      if(slugwall){
        let wl = profileUser.walls.filter(it=> it.slug === slugwall);
        wl = wl.length ? wl[0] : null;
        if(wl){
          console.log('ProfilePagesds', slugwall, profileUser, items);
          items = globalItems.filter(it=>it.wall_id===wl.id).sort((a,b)=> a.created - b.created).reverse();
        }
      }
    }
    if(profileUser && profileUser.is_site){
      items = items.filter(item =>  item.site_id === profileUser.id).sort((a,b)=> a.created - b.created)
    }

    return (
      <Screen header={<Header user={user} {...this.props}/>} children={
        <Scrollbars className='' style={{ width: '100%', height: '100%'}}>
          <div className="pf-container">
            <div className="pf-container-inner">
              <div className="pf-container-left">
                {loadingProfile && (user && user !== profileUser) ?
                  <LazyLoading  {...this.props} className='lf_column lazy__column'>
                    <div className="pf_column__top">
                      <div className="pf_column__top__title">
                        <div className="lazy__title"/>
                      </div>
                      <div className="pf_column__top__username">
                        <div style={{height: '10px'}} className="lazy__title"/>
                      </div>
                    </div>
                    <div className="pf_column__avatar">
                      <div className="lazy__img"/>
                    </div>
                    <div className="pf_column__info">
                      <div className="pf_column__info__title">
                        <div style={{height: '10px'}} className="lazy__title"/>
                      </div>
                    </div>
                  </LazyLoading>
                  : <Profile color={color}
                             username={username}
                             colorOpacity={colorOpacity}
                             backOpacity={backOpacity}
                             profileUser={profileUser} user={user} {...this.props}/>}

              </div>
              <div className="pf-container-right">
                <FeedItem noscroll={true} items={items} user={user} {...this.props}/>
              </div>
            </div>
          </div>
        </Scrollbars>
      }>
      </Screen>
    );
  }
}