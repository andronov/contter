import * as React from 'react';
import { observer , inject} from 'mobx-react';
import './style.scss';
import Screen from '../Screen';
import Header from '../Header';
import { Scrollbars } from 'react-custom-scrollbars';
import LeftManageColumn from '../LeftManageColumn';
import MiniCard from '../MiniCard';

@inject("appState")
@observer
export default class FollowersPage extends React.Component {

  componentDidMount = () => {
    const { appState} = this.props;
    let user = appState.currentUser;
    user && user.getFollowers(0, 15)
  };

  render() {
    const { appState} = this.props;
    let user = appState.currentUser;

    if(!user)return(<div></div>);

    let color = appState.get_color(1, null, user.color);
    let colorOpacity = appState.get_color('0.20', null, user.color);
    let backOpacity = appState.get_color( '0.07', null, user.color);
    let img = 'https://s3-us-west-2.amazonaws.com/contter/site/20170228/148b1738-eff5-4477-adfe-5ab2c352a64b-facebook_app_logo_sports.png';

    return (
      <Screen header={<Header user={user} {...this.props}/>} children={
        <Scrollbars className='' style={{ width: '100%', height: '100%'}}>
          <div className="cn-container">
            <div className="cn-container-inner">

              <div style={{minWidth: '190px'}} className="cn-container-left">
                <LeftManageColumn user={user} followers={true} activeFooter={false} color={color} colorOpacity={colorOpacity}
                                  backOpacity={backOpacity} {...this.props}/>
              </div>

              <div style={{width: '100%'}} className="cn-container-right">
                <div style={{width: '100%'}} className="cn-container-followers">
                  {user.followers.map(follow => {
                    return(
                      <MiniCard key={follow.id} type="profile"
                                title={follow.first_name}
                                slug={follow.username}
                                image={`https://s3-us-west-2.amazonaws.com/contter/${follow.avatar}`}
                                user={user} {...this.props}/>
                    )
                  })}
                </div>
              </div>
            </div>
          </div>
        </Scrollbars>
      }>
      </Screen>
    );
  }
}