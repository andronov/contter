import { observable, computed } from 'mobx';
import BaseClass from './BaseClass';
import { rgba } from '../services/utils';
import { GET_USER_URL, GET_SOURCES_WAlL} from '../services/constants';
import { RestApi } from '../services/RestApi';
import User from './User';
import Site from './Site';

import Router from './Router';

class AppState extends BaseClass {
  router = Router.create();

  @observable currentUser;
  @observable users = [];

  @observable app_side = 'main';
  @observable app_content = 'profile';

  @observable shareItem = null;
  @observable shareOpen = false;

  @observable pageSize;
  @observable searchWallSources = [];
  @observable loadingAddWallSearch = false;
  @observable errorAddWallSearch = false;
  @observable noElseAddWallSearch = false;
  @observable color = null;
  @observable colorOpacity = null;
  @observable backOpacity = null;

  @observable savingAddWall = false;
  @observable errorAddWall = false;

  @observable savingEditWall = false;
  @observable errorEditWall = false;

  @observable loadingProfile = false;
  @observable loadingGlobalItems = false;

  @observable colors = [
    {'id': 0, 'color': 'e53935'},
    {'id': 0, 'color': 'eb3f79'},
    {'id': 0, 'color': 'aa00ff'},
    {'id': 0, 'color': '7d58c2'},
    {'id': 0, 'color': '5b6abf'},
    {'id': 0, 'color': '1c88e3'},
    {'id': 0, 'color': '029ae5'},
    {'id': 0, 'color': '00abbf'},
    {'id': 0, 'color': '00887a'},
    {'id': 0, 'color': '679e38'},
    {'id': 0, 'color': 'f8a724'},
    {'id': 0, 'color': 'ff6f42'},
    {'id': 0, 'color': '8b6d62'},
    {'id': 0, 'color': '778f9b'},
    {'id': 0, 'color': '414141'},

    {'id': 0, 'color': 'e53932'},
  ];

  isMobile() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  }

  async getUser(username) {
    RestApi({username: username}, GET_USER_URL).then(response => {
      console.log('getUser', response.data);
      let user = User.create(response.data.data);
      this.users.push(user);
      this.loadingProfile = false;
      user.loadItems('#', 0, 15);
      if(user !== this.currentUser) {
        user.loadAllUserWalls();
        if(user.is_site){
          user.loadSuggestUsers();
        }
      }

      /*response.data.walls.slice().forEach(wl => {
        Wall.create({...wl, user: this});
      });*/
    }).catch(error => {
      this.loadingProfile = false;
      console.log('error', error)
    });
  }

  async getAddWallSearch(query) {
    this.update({loadingAddWallSearch: true,
      searchWallSources: []});
    RestApi({query: query}, GET_SOURCES_WAlL).then(response => {
      console.log('getAddWallSearch', response.data);
      response.data.data.slice().forEach(item => {
        if(item.type == 'SITE'){
          this.searchWallSources.push(Site.create({...item}))
        }
        else{
          this.searchWallSources.push(User.create({...item}))
        }
      });
    }).catch(error => {
      this.update({loadingAddWallSearch: false,
        errorAddWallSearch: true});
      console.log('error', error)
    });
  }

  get_color = (opacity, item, cl) => {
    // TODO сделать проверку на стены и на чужих профилях
    let color = 'fff';
    if(item){
      color = item.color
    }
    else if(this.currentUser && this.currentUser.activeWall){
      color = this.currentUser.activeWall.color;
    }
    else if(this.users.filter(us => us.username === this.get_userpath()).length){
      color = this.users.filter(us => us.username === this.get_userpath())[0].color
    }
    else if(this.currentUser){
      color = this.currentUser.color;
    }
    if(cl){color = cl}
    /*let c = rgba(color, opacity ? opacity: 1);
    switch(opacity) {
      case 1:
        this.color = c;
        break;
      case '0.20':
        this.colorOpacity = c;
        break;
      case '0.07':
        this.backOpacity = c;
        break;
    }*/
    return rgba(color, opacity ? opacity: 1)
  };

  get_userpath = () => {
    if(window.matchRoute && window.matchRoute.path.startsWith('/:username')){
      return window.matchRoute.params.username
    }
    return ''
  }

}

export default AppState.create();
