import { observable, computed } from 'mobx';
import BaseClass from './BaseClass';

export default class Rating extends BaseClass {
  static table = 'rating';


  @observable dislikes;
  @observable likes;
  @observable score;
  @observable unvoted;
  @observable your;
}