import appState from 'stores/AppState';
import { autorun } from 'mobx';
import runtime from 'serviceworker-webpack-plugin/lib/runtime';

export function registerServiceWorker() {
  if ('serviceWorker' in navigator) {
    let newWorker = null;


    // navigator.serviceWorker.register('service-worker.js')
    Promise.resolve(runtime.register())
      .then(reg => {
        if (!navigator.serviceWorker.controller) {
          console.log('--swdebug no controller');
          return;
        }

        if (reg.waiting) {
          console.log('--swdebug waiting');
          newWorker = reg.waiting;
          updateReady();
          // for test
          newWorker.postMessage({ action: 'skipWaiting' });
          return;
        }

        if (reg.installing) {
          console.log('--swdebug installing');
          newWorker = reg.installing;
          trackInstalling(newWorker);
          return;
        }

        reg.addEventListener('updatefound', () => {
          console.log('--swdebug updatefound');
          newWorker = reg.installing;
          trackInstalling(newWorker);
        });
      }).then(() => {
        if (navigator.serviceWorker.controller) {
          console.log('--swdebug controller', navigator.serviceWorker);
          connectToWebsockets();
        }

        navigator.serviceWorker.onmessage = (event) => {
          console.log('sw-log MESSAGE FROM SW', event.data);
       /* let payload = event.data.payload;
        let messageData = null,
            newMessage = null;
        if(payload.action == 'message_add' || payload.editMessage){
          messageData = payload.editMessage ? payload.editMessage.message : payload.createMessage.message;
          console.log('from websockets', messageData);
          if(!messageData.parent){
            messageData.parent = appState.currentUser.selectedThread.rootMessage;
          }
          newMessage = Message.create(messageData);
          n98ewMessage.recipient.updateScroll();
        }*/
        };

        navigator.serviceWorker.oncontrollerchange = () => {
          console.log('--swdebug oncontrollerchange');
          if (newWorker) {
            location.reload();
          } else {
            console.log('controller2', navigator.serviceWorker);
            connectToWebsockets();
          }
        };

        let previousIsAction;
        autorun(() => {
          if (appState.snackBar.isAction !== previousIsAction) {
            if (appState.snackBar.isAction && appState.snackBar.action == 'refresh' && newWorker) {
              newWorker.postMessage({ action: 'skipWaiting' });
            }
            previousIsAction = appState.snackBar.isAction;
          }
        });
      })
      .then(askNotificationPermission)
      .catch(err => console.error(err));
  }
}

export function sendMessage(data) {
  return new Promise((resolve, reject) => {
    const messageChannel = new MessageChannel();
    messageChannel.port1.onmessage = (event) => {
      if (event.data.error) {
        reject(event.data.error);
      } else {
        resolve(event.data);
      }
    };
    navigator.serviceWorker.controller.postMessage(data, [messageChannel.port2]);
  });
}

function connectToWebsockets() {
   /* return sendMessage({
      action: 'createMQTTConnection',
      jwt: AuthStorage.getSession().jwt,
      documentUrl: location.protocol + '//' + location.host
    });*/
  /* console.log('connectToWebsockets', navigator);

  navigator.serviceWorker.controller.postMessage({
    action: 'createMQTTConnection',
    jwt: AuthStorage.getSession().jwt,
    currentUser: AppState.currentUser,
    documentUrl: location.protocol + '//' + location.host
  });*/
}

function askNotificationPermission() {
  if (!('Notification' in window)) {
    throw new Error('notification not supported')
  }

  return Notification.requestPermission()
    .then(permissionResult => {
      if (permissionResult !== 'granted') {
        throw new Error('We weren\'t granted permission.');
      }
    })
}

function updateReady() {
  appState.update({
    isOpen: true,
    isAction: false,
    message: 'New version available',
    action: 'refresh',
  });
}

function trackInstalling(worker) {
  worker.addEventListener('statechange', () => {
    if (worker.state == 'installed') {
      updateReady();
    }
  });
}
