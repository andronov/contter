import { isAuthenticated, getToken, setToken, getAuthHeader } from './shared'
import { login, logout, signup, refreshToken } from './local'
import { getProfile, updateProfile } from './user'

import Facebook from './components/Facebook'
export { Facebook }
import Twitter from './components/Twitter'
export { Twitter }
import Google from './components/Google'
export { Google }

import Auth from './components/Auth'
export {Auth}

export default {
	Auth,
	Facebook,
  Twitter,
	Google,
	isAuthenticated,
	login,
	logout,
	signup,
	getToken,
	setToken,
	refreshToken,
	getAuthHeader,
	getProfile,
	updateProfile
}