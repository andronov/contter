import moment from 'moment';

export default locale => {
  moment.locale(locale);
  global.moment = moment;
  return moment;
};
