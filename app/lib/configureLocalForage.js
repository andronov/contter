import localForage from 'localforage';
export default driver => {
  const storage = localForage.createInstance({
    name: 'appState',
    version: 1,
    description: 'appState persist',
  });

  return storage;
};
